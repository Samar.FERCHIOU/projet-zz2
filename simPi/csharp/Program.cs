using System;
using System.Collections.Generic;
using System.IO;
using PseudoRandom;

class Program {

    public static MersenneTwister mt;
    public static double CalculatePi(int numberOfPoints, MersenneTwister mt)
    {
        int count = 0;
        for (int i = 0; i < numberOfPoints; ++i)
        {
            double xr = mt.genrand_real2(); 
            double yr = mt.genrand_real2();
            if (xr * xr + yr * yr <= 1)
            {
                count++;
            }
        }
        return 4.0 * count / numberOfPoints;
    }
    static void Main()
    {
        mt = new MersenneTwister();
        int numberOfPoints = 1000000000;

        double pi = CalculatePi(numberOfPoints, mt);
        Console.WriteLine("With 1,000,000,000 points: {0:F10}", pi);


    }

   
}
