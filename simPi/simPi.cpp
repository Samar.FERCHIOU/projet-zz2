#include <iostream>
#include <iomanip>
#include "../MersenneTwister/MersenneTwister.hpp"


double estimatePi(int numberOfPoints, MersenneTwister* mt) {
    int count = 0;
    for (int i = 0; i < numberOfPoints; ++i) {
        double xr = mt->genrand_real2();
        double yr = mt->genrand_real2();

        if (xr * xr + yr * yr <= 1) {
            count++;
        }
    }
    return 4.0 * count / numberOfPoints;
}

int main() {
    uint32_t init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
 	mt = new MersenneTwister(init, length);
    
    int numberOfPoints = 1000000000;
    
    double pi = estimatePi(numberOfPoints, mt);
    std::cout << "With 1,000,000,000 points: " << std::fixed << std::setprecision(10) << pi << std::endl;
    
    return 0;
}
