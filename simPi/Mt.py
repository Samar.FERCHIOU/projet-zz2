N = 624
M = 397
MATRIX_A = 0x9908b0df
UPPER_MASK = 0x80000000
LOWER_MASK = 0x7fffffff

class MersenneTwister:
    def __init__(self):
        self.mt = [0] * N
        self.mti = N + 1

    def init_genrand(self, s):
        self.mt[0] = s & 0xffffffff
        for self.mti in range(1, N):
            self.mt[self.mti] = (1812433253 * (self.mt[self.mti - 1] ^ (self.mt[self.mti - 1] >> 30)) + self.mti)
            self.mt[self.mti] &= 0xffffffff


    def init_by_array(self, init_key, key_length):
        i, j, k = 1, 0, 0
        self.init_genrand(19650218)
        k = N if N > key_length else key_length

        for _ in range(k):
            self.mt[i] = (self.mt[i] ^ ((self.mt[i - 1] ^ (self.mt[i - 1] >> 30)) * 1664525)) + init_key[j] + j
            self.mt[i] &= 0xffffffff
            i += 1
            j += 1
            if i >= N:
                self.mt[0] = self.mt[N - 1]
                i = 1
            if j >= key_length:
                j = 0

        for _ in range(N - 1):
            self.mt[i] = (self.mt[i] ^ ((self.mt[i - 1] ^ (self.mt[i - 1] >> 30)) * 1566083941)) - i
            self.mt[i] &= 0xffffffff
            i += 1
            if i >= N:
                self.mt[0] = self.mt[N - 1]
                i = 1

        self.mt[0] = 0x80000000
    


    def genrand_int32(self):
        mag01 = [0x0, MATRIX_A]

        if self.mti >= N:
            if self.mti == N + 1:
                self.init_genrand(5489)
            
            for kk in range(N - M):
                y = (self.mt[kk] & UPPER_MASK) | (self.mt[kk + 1] & LOWER_MASK)
                self.mt[kk] = self.mt[kk + M] ^ (y >> 1) ^ mag01[y & 0x1]

            for kk in range(N - M, N - 1):
                y = (self.mt[kk] & UPPER_MASK) | (self.mt[kk + 1] & LOWER_MASK)
                self.mt[kk] = self.mt[kk + (M - N)] ^ (y >> 1) ^ mag01[y & 0x1]
            
            y = (self.mt[N - 1] & UPPER_MASK) | (self.mt[0] & LOWER_MASK)
            self.mt[N - 1] = self.mt[M - 1] ^ (y >> 1) ^ mag01[y & 0x1]

            self.mti = 0
        
        y = self.mt[self.mti]
        self.mti += 1

        y ^= (y >> 11)
        y ^= (y << 7) & 0x9d2c5680
        y ^= (y << 15) & 0xefc60000
        y ^= (y >> 18)

        return y

    def genrand_real1(self):
        return self.genrand_int32() * (1.0 / 4294967295.0)

    def genrand_real2(self):
        return self.genrand_int32() * (1.0 / 4294967296.0)

    def genrand_real3(self):
        return ((self.genrand_int32()) + 0.5) * (1.0 / 4294967296.0)

    def genrand_res53(self):
        a = self.genrand_int32() >> 5
        b = self.genrand_int32() >> 6
        return (a * 67108864.0 + b) * (1.0 / 9007199254740992.0)

#init
# init_key = [0x123, 0x234, 0x345, 0x456]
# mt = MersenneTwister()
# mt.init_by_array(init_key,4)
        
# x = mt.genrand_int32() 
