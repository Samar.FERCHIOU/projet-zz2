#!/bin/bash

# Name of the files to store results
# #TIME_FILE="time_results/time_results_python.txt"
# POWER_FILE="power_results/power_results_python.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # Python implementation
# #for i in {1..2}
# #do
#     # Measure the time using built-in time and append to time_results.txt
#    # (time python3 simPi.py) 2>> $TIME_FILE

# #done

# for i in {1..2}
#  do
#      # powerjoular measures the power consumption while running your_python_script.py
#      # Append the result to power_results.txt
#      python3 simPi.py &
#      PYTHON_PID=$!
#      powerjoular -p $PYTHON_PID >> $POWER_FILE &
#      POWERJOULAR_PID=$!
#      sleep 1680
#      kill -INT $POWERJOULAR_PID
#      kill $PYTHON_PID

#  done


############################################
############################################
############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_c.txt"
# POWER_FILE="power_results/power_results_c.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation
# gcc simPi.c -o simPiC
# #for i in {1..5}
# #do
#  #   (time ./simPiC) 2>> $TIME_FILE
# #done

#  for i in {1..5}
#  do
#      ./simPiC &
#      PROGRAM_PID=$!
#      powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#      POWERJOULAR_PID=$!
#      sleep 20
#      kill -INT $POWERJOULAR_PID
#      kill $PROGRAM_PID
#  done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_cO2.txt"
# POWER_FILE="power_results/power_results_cO2.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation -o2
# gcc -o2 simPi.c -o simPiCo2
# # for i in {1..5}
# # do
# #     (time ./simPiCo2) 2>> $TIME_FILE
# # done

# for i in {1..5}
# do
#     ./simPiCo2 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 22
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_cO3.txt"
# POWER_FILE="power_results/power_results_cO3.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation -o3
# gcc -o3 simPi.c -o simPiCo3
# # for i in {1..5}
# # do
# #     (time ./simPiCo3) 2>> $TIME_FILE
# # done

# for i in {1..5}
# do
#     ./simPiCo3 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 22
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_cpp.txt"
#POWER_FILE="power_results/power_results_cpp.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# C++ implementation
#g++ simPi.cpp ../MersenneTwister/MersenneTwister.cpp -o simPiCpp

#for i in {1..5}
#do
 #   (time ./simPiCpp) 2>> $TIME_FILE
#done

# for i in {1..5}
# do
#     ./simPiCpp &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 25
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_cppO2.txt"
# POWER_FILE="power_results/power_results_cppO2.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C++ implementation -o2
# g++ -o2 simPi.cpp ../MersenneTwister/MersenneTwister.cpp -o simPiCppO2

# # for i in {1..5}
# # do
# #     (time ./simPiCppO2) 2>> $TIME_FILE
# # done

# for i in {1..5}
# do
#     ./simPiCppO2 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 25
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_cppO3.txt"
# POWER_FILE="power_results/power_results_cppO3.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C++ implementation -o3
# g++ -o3 simPi.cpp ../MersenneTwister/MersenneTwister.cpp -o simPiCppO3

# # for i in {1..5}
# # do
# #     (time ./simPiCppO3) 2>> $TIME_FILE
# # done

# for i in {1..5}
# do
#     ./simPiCppO3 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 25
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_java.txt"
POWER_FILE="power_results/power_results_java.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# Java implementation
#javac simPi.java

#for i in {1..5}
#do
#    (time java simPi) 2>> $TIME_FILE
#done

# for i in {1..5}
# do
#     java simPi &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 38
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

ORIGINAL_DIR=$(pwd)
# Name of the files to store results
TIME_FILE="$ORIGINAL_DIR/time_results/time_results_csharp.txt"
POWER_FILE="$ORIGINAL_DIR/power_results/power_results_csharp.txt"

# Clean any existing result files
#> $TIME_FILE
> $POWER_FILE

cd csharp

# Csharp implementation
/home/samar/.dotnet/dotnet build

#for i in {1..5}
#do
#   (time /home/samar/.dotnet/dotnet run) 2>> $TIME_FILE
#done

 for i in {1..5}
 do
     /home/samar/.dotnet/dotnet run &
     PROGRAM_PID=$!
     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
     POWERJOULAR_PID=$!
     sleep 60
     kill -INT $POWERJOULAR_PID
     kill -INT $PROGRAM_PID
 done

cd "$ORIGINAL_DIR"


