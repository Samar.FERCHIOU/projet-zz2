import java.util.Random;

public class simPi {

    // Initialisation de la graine
   // private static final long SEED = 12345;

    public static void main(String[] args) {
        int numberOfPoints = 1000000000;
        
        int[] tab = {0x123, 0x234, 0x345, 0x456};
        MTRandom rand = new MTRandom(tab);

       double pi = estimatePi(numberOfPoints, rand);
       System.out.printf("With 1,000,000,000 points: %.10f%n", pi);

    }

    public static double estimatePi(int numberOfPoints, MTRandom rand) {
        int count = 0;
        double xr;
        double yr;
        long unsignedInt;

        for (int i = 0; i < numberOfPoints; i++) {
            unsignedInt = (rand.nextInt() & 0xFFFFFFFFL);
            xr = unsignedInt / (double) (0xFFFFFFFFL); 
            unsignedInt = (rand.nextInt() & 0xFFFFFFFFL);
            yr = unsignedInt / (double) (0xFFFFFFFFL); 
            // xr = rand.nextFloat();
            // yr = rand.nextFloat();
            
            if (xr * xr + yr * yr <= 1) {
                count++;
            }
        }
        
        return 4.0 * count / numberOfPoints;
    }
}
