from Mt import MersenneTwister

def estimate_pi(number_of_points, mt):

    count = 0
    for _ in range(number_of_points):
        xr = mt.genrand_real2()
        yr = mt.genrand_real2()
        #print(xr, yr)
        if xr * xr + yr * yr <= 1:
            count += 1

    return 4.0 * count / number_of_points

def main(): 
    init_key = [0x123, 0x234, 0x345, 0x456]
    mt = MersenneTwister()
    mt.init_by_array(init_key,4)
            
    x = mt.genrand_int32() 

    number_of_points = 1000000000
    
    pi = estimate_pi(number_of_points, mt)
    print(f"With 1,000,000,000 points: {pi:.10f}")

if __name__ == "__main__":
    main()
