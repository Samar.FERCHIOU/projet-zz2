program nsieve
  implicit none
  integer :: m, i, j, count
  logical, allocatable :: flags(:)

  m = atoi(command_argument(1))

  do i = 0, 2
    call nsieve(10000 * 2**(m - i))
  end do

contains

  subroutine nsieve(m)
    integer, intent(in) :: m
    integer :: count, i, j
    logical, allocatable :: flags(:)

    allocate(flags(m))
    flags = .true.

    count = 0
    do i = 2, m - 1
      if (flags(i)) then
        count = count + 1
        do j = i * 2, m - 1, i
          flags(j) = .false.
        end do
      end if
    end do

    deallocate(flags)
    print *, 'Primes up to ', m, count
  end subroutine nsieve

end program nsieve
