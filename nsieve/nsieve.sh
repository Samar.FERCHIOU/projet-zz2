#!/bin/bash

# Name of the files to store results
TIME_FILE="time_results/time_results_python.txt"
#POWER_FILE="power_results/power_results_python.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# Python implementation
for i in {1..5}
do
    # Measure the time using built-in time and append to time_results.txt
    (time python3 1.py 13) 2>> $TIME_FILE

done

#for i in {1..30}
# do
#     python3 1.py 13 &
#     PYTHON_PID=$!
#     powerjoular -p $PYTHON_PID >> $POWER_FILE_PYTHON &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PYTHON_PID

# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_c.txt"
#POWER_FILE="power_results/power_results_c.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C implementation
gcc 1.c -o r1
for i in {1..5}
do
    (time ./r1 13) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./executableC &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_C &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cO2.txt"
#POWER_FILE="power_results/power_results_cO2.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C implementation -o2
gcc -o2 1.c -o r1
for i in {1..5}
do
    (time ./r1 13) 2>> $TIME_FILE
done

# for i in {1..5}
# do
#     ./r1 13 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_C &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cO3.txt"
#POWER_FILE="power_results/power_results_cO3.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C implementation -o3
gcc -o3 1.c -o r1
for i in {1..5}
do
    (time ./r1 13) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./executableC &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_C &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cpp.txt"
#POWER_FILE="power_results/power_results_cpp.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C++ implementation
g++ 1.cpp -o 1Cpp

for i in {1..5}
do
    (time ./1Cpp 13) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./1Cpp 13 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_CPP &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cppO2.txt"
#POWER_FILE="power_results/power_results_cppO2.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C++ implementation
g++ -o2 1.cpp -o 1CppO2

for i in {1..5}
do
    (time ./1CppO2 13) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./1CppO2 13 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_CPP &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cppO3.txt"
#POWER_FILE="power_results/power_results_cppO3.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C++ implementation
g++ -o3 1.cpp -o 1CppO3

for i in {1..5}
do
    (time ./1CppO3 13) 2>> $TIME_FILE
done

# for i in {1..5}
# do
#     ./1Cpp 13 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_CPP &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_java.txt"
#POWER_FILE="power_results/power_results_java.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# Java implementation
javac app.java

for i in {1..5}
do

    (time java app 13) 2>> $TIME_FILE
done

# for i in {1..8}
# do
#     java app 13 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_JAVA &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done


############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_ruby.txt"
#POWER_FILE="power_results/power_results_ruby.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# Ruby implementation
for i in {1..3}
do

    (time ruby 1.rb 13) 2>> $TIME_FILE
done

# for i in {1..3}
# do
#     ruby 1.rb 13 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_JAVA &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done
