using System;
using PseudoRandom;

class Program {
	private void Main_(string[] args) {
		MersenneTwister mersenneTwister = new MersenneTwister();
//		ulong[] init = new ulong[] { 0x123, 0x234, 0x345, 0x456 };
//		mersenneTwister.init_by_array(init);
		Console.Write("1000 outputs of genrand_int32()\n");
		for (int i = 0; i < 1000; i++) {
			Console.Write("{0:0000000000} ", mersenneTwister.genrand_uint32());
			if (i % 5 == 4) { Console.Write("\n"); }
		}
		Console.Write("\n1000 outputs of genrand_real2()\n");
		for (int i = 0; i < 1000; i++) {
			Console.Write("{0:0.00000000} ", mersenneTwister.genrand_real2());
			if (i % 5 == 4) { Console.Write("\n"); }
		}
		for (int i = 0; i < 1000; i++) {
			Console.Write("{0} ", mersenneTwister.genrand_N(5));
			if (i % 5 == 4) { Console.Write("\n"); }
		}
	}
	static void Main(string[] args) {
		Program program = new Program();
		program.Main_(args);
	}
}
