class Graph:
    def __init__(self, V):
        self.V = V
        self.adj = [[] for _ in range(V)]

    # Ajouter une arête au graphe
    def addEdge(self, u, v):
        self.adj[u].append(v)
        self.adj[v].append(u) # Pour un graphe non dirigé

    # Parcours en profondeur à partir d'un sommet donné
    def DFSUtil(self, start, visited, path):
        visited[start] = True # Marquer le sommet de départ comme visité
        path.append(start) # Ajouter le sommet de départ au chemin parcouru

        # Parcourir tous les sommets adjacents du sommet actuel
        for neighbor in self.adj[start]:
            if not visited[neighbor]:
                self.DFSUtil(neighbor, visited, path) # Récursivement parcourir le voisin non visité

    def DFS(self, start):
        visited = [False] * self.V # Liste pour suivre les sommets visités
        path = [] # Liste pour stocker le chemin parcouru

        self.DFSUtil(start, visited, path) # Appel récursif de la fonction utilitaire DFS

        # Afficher le chemin parcouru
        print("Chemin parcouru :", end=" ")
        for node in path:
            print(node, end=" ")
        print()

# Création d'un graphe non dirigé avec 20 sommets
g = Graph(2000)
# Ajout d'arêtes au graphe
# g.addEdge(0, 1)
# g.addEdge(0, 2)
# g.addEdge(1, 3)
# g.addEdge(1, 4)
# g.addEdge(2, 5)
# g.addEdge(2, 6)
# g.addEdge(3, 7)
# g.addEdge(3, 8)
# g.addEdge(4, 9)
# g.addEdge(5, 10)
# g.addEdge(5, 11)
# g.addEdge(6, 12)
# g.addEdge(6, 13)
# g.addEdge(7, 14)
# g.addEdge(7, 15)
# g.addEdge(8, 16)
# g.addEdge(8, 17)
# g.addEdge(9, 18)
# g.addEdge(9, 19)

for i in range(1000):
    g.addEdge(i, (i * 2 + 1) % 2000)
    g.addEdge(i, (i * 2 + 2) % 2000)

print("Parcours DFS à partir du sommet 0 :", end=" ")
g.DFS(0) # Parcours DFS à partir du sommet 0
