class Graph
    def initialize(v)
      @v = v
      @adj = Array.new(v) { [] }
    end
  
    def add_edge(u, v)
      @adj[u] << v
      @adj[v] << u # Pour un graphe non dirigé
    end
  
    def dfs_util(start, visited, path)
      visited[start] = true # Marquer le sommet de départ comme visité
      path << start # Ajouter le sommet de départ au chemin parcouru
  
      # Parcourir tous les sommets adjacents du sommet actuel
      @adj[start].each do |neighbor|
        unless visited[neighbor]
          dfs_util(neighbor, visited, path) # Récursivement parcourir le voisin non visité
        end
      end
    end
  
    def dfs(start)
      visited = Array.new(@v, false) # Liste pour suivre les sommets visités
      path = [] # Liste pour stocker le chemin parcouru
  
      dfs_util(start, visited, path) # Appel récursif de la fonction utilitaire DFS
  
      # Afficher le chemin parcouru
      print "Chemin parcouru : "
      path.each { |node| print "#{node} " }
      puts
    end
  end
  
  # Création d'un graphe non dirigé avec 20 sommets
  g = Graph.new(2000)
  # Ajout d'arêtes au graphe
  # g.add_edge(0, 1)
  # g.add_edge(0, 2)
  # g.add_edge(1, 3)
  # g.add_edge(1, 4)
  # g.add_edge(2, 5)
  # g.add_edge(2, 6)
  # g.add_edge(3, 7)
  # g.add_edge(3, 8)
  # g.add_edge(4, 9)
  # g.add_edge(5, 10)
  # g.add_edge(5, 11)
  # g.add_edge(6, 12)
  # g.add_edge(6, 13)
  # g.add_edge(7, 14)
  # g.add_edge(7, 15)
  # g.add_edge(8, 16)
  # g.add_edge(8, 17)
  # g.add_edge(9, 18)
  # g.add_edge(9, 19)

  (0...1000).each do |i|
    g.add_edge(i, (i * 2 + 1) % 2000)
    g.add_edge(i, (i * 2 + 2) % 2000)
  end
  
  print "Parcours DFS à partir du sommet 0 : "
  g.dfs(0) # Parcours DFS à partir du sommet 0
  