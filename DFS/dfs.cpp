#include <iostream>
#include <vector>
#include <stack>

using namespace std;

// Structure pour représenter un graphe non dirigé par une liste d'adjacence
class Graph {
    int V; // Nombre de sommets
    vector<vector<int>> adj; // Liste d'adjacence

public:
    Graph(int V) : V(V) {
        adj.resize(V);
    }

    // Ajouter une arête au graphe
    void addEdge(int u, int v) {
        adj[u].push_back(v);
        adj[v].push_back(u); // Pour un graphe non dirigé
    }

    // Parcours en profondeur à partir d'un sommet donné
    void DFSUtil(int start, vector<bool>& visited, vector<int>& path) {
        visited[start] = true; // Marquer le sommet de départ comme visité
        path.push_back(start); // Ajouter le sommet de départ au chemin parcouru

        // Parcourir tous les sommets adjacents du sommet actuel
        for (int neighbor : adj[start]) {
            if (!visited[neighbor]) {
                DFSUtil(neighbor, visited, path); // Récursivement parcourir le voisin non visité
            }
        }
    }

    void DFS(int start) {
        vector<bool> visited(V, false); // Tableau pour suivre les sommets visités
        vector<int> path; // Vecteur pour stocker le chemin parcouru

        DFSUtil(start, visited, path); // Appel récursif de la fonction utilitaire DFS

        // Afficher le chemin parcouru
        cout << "Chemin parcouru : ";
        for (int node : path) {
            cout << node << " ";
        }
        cout << endl;
    }

    void printGraph() {
        for (int i = 0; i < V; ++i) {
            cout << i << ": ";
            for (int j = 0; j < adj[i].size(); ++j) {
                cout << adj[i][j];
                if (j < adj[i].size() - 1)
                    cout << ", ";
            }
            cout << endl;
        }
    }
};

int main() {
    // Création d'un graphe non dirigé avec 20 sommets
    Graph g(2000);
    // Ajout d'arêtes au graphe
    // g.addEdge(0, 1);
    // g.addEdge(0, 2);
    // g.addEdge(1, 3);
    // g.addEdge(1, 4);
    // g.addEdge(2, 5);
    // g.addEdge(2, 6);
    // g.addEdge(3, 7);
    // g.addEdge(3, 8);
    // g.addEdge(4, 9);
    // g.addEdge(5, 10);
    // g.addEdge(5, 11);
    // g.addEdge(6, 12);
    // g.addEdge(6, 13);
    // g.addEdge(7, 14);
    // g.addEdge(7, 15);
    // g.addEdge(8, 16);
    // g.addEdge(8, 17);
    // g.addEdge(9, 18);
    // g.addEdge(9, 19);
    
    for (int i = 0; i < 1000; ++i) {
        g.addEdge(i, (i * 2 + 1) % 2000);
        g.addEdge(i, (i * 2 + 2) % 2000);
    }

    g.printGraph();

    cout << "Parcours DFS à partir du sommet 0 : ";
    g.DFS(0); // Parcours DFS à partir du sommet 0
    cout << endl;

    return 0;
}
