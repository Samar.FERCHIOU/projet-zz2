#include <stdio.h>
#include <stdlib.h>

#define MAX_VERTICES 2000

// Structure pour représenter un graphe non orienté à l'aide d'une liste d'adjacence
typedef struct {
    int V; // Nombre de sommets
    int* adj[MAX_VERTICES]; // Liste d'adjacence
} Graph;

void addEdge(Graph* g, int u, int v) {
    // Assurer que u et v sont des sommets valides
    if (u < 0 || u >= MAX_VERTICES || v < 0 || v >= MAX_VERTICES) {
        fprintf(stderr, "Sommets invalides\n");
        exit(EXIT_FAILURE);
    }

    // Allouer de la mémoire pour la liste d'adjacence du sommet u si elle est NULL
    if (g->adj[u] == NULL) {
        g->adj[u] = (int*)malloc(2 * sizeof(int)); // Allouer de l'espace pour deux entiers
        if (g->adj[u] == NULL) {
            fprintf(stderr, "Allocation mémoire échouée\n");
            exit(EXIT_FAILURE);
        }
        g->adj[u][0] = v; // Ajouter le sommet v à la liste d'adjacence de u
        g->adj[u][1] = -1; // Valeur sentinelle pour indiquer la fin de la liste
        g->V++; // Incrémenter le nombre de sommets
    }
    else {
        // Trouver l'emplacement disponible dans la liste d'adjacence
        int i = 0;
        while (g->adj[u][i] != -1) {
            if (g->adj[u][i] == v) {
                // L'arête existe déjà
                return;
            }
            i++;
        }
        g->adj[u] = (int*)realloc(g->adj[u], (i + 2) * sizeof(int));
        if (g->adj[u] == NULL) {
            fprintf(stderr, "Réallocation mémoire échouée\n");
            exit(EXIT_FAILURE);
        }
        g->adj[u][i] = v; // Ajouter le sommet v à la liste d'adjacence de u
        g->adj[u][i + 1] = -1; // Valeur sentinelle pour indiquer la fin de la liste
    }

    // Faire de même pour le sommet v
    if (g->adj[v] == NULL) {
        g->adj[v] = (int*)malloc(2 * sizeof(int));
        if (g->adj[v] == NULL) {
            fprintf(stderr, "Allocation mémoire échouée\n");
            exit(EXIT_FAILURE);
        }
        g->adj[v][0] = u;
        g->adj[v][1] = -1;
        g->V++;
    }
    else {
        // Trouver l'emplacement disponible dans la liste d'adjacence
        int i = 0;
        while (g->adj[v][i] != -1) {
            if (g->adj[v][i] == u) {
                // L'arête existe déjà
                return;
            }
            i++;
        }
        g->adj[v] = (int*)realloc(g->adj[v], (i + 2) * sizeof(int));
        if (g->adj[v] == NULL) {
            fprintf(stderr, "Réallocation mémoire échouée\n");
            exit(EXIT_FAILURE);
        }
        g->adj[v][i] = u;
        g->adj[v][i + 1] = -1;
    }
}


// Depth-first search from a given vertex
// In DFSUtil function, iterate over only the adjacent vertices
void DFSUtil(Graph* g, int start, int visited[]) {
    visited[start] = 1; // Mark the current vertex as visited
    printf("%d ", start); // Print the current vertex

    // Iterate over the adjacent vertices
    for (int i = 0; g->adj[start][i] != -1; ++i) {
        int adj_vertex = g->adj[start][i];
        if (!visited[adj_vertex]) {
            DFSUtil(g, adj_vertex, visited);
        }
    }
}

void DFS(Graph* g, int start) {
    int* visited = (int*)calloc(g->V, sizeof(int)); // Array to track visited vertices, initialized to 0
    if (visited == NULL) {
        fprintf(stderr, "Memory allocation failed\n");
        exit(EXIT_FAILURE);
    }

    DFSUtil(g, start, visited); // Call the utility function DFS
    free(visited); // Free the memory allocated for the visited array
}

int main() {
    // Initialiser le graphe
    Graph g;
    g.V = 0;
    for (int i = 0; i < MAX_VERTICES; ++i)
        g.adj[i] = NULL;

    // Ajouter des arêtes au graphe
    // addEdge(&g, 0, 1);
    // addEdge(&g, 0, 2);
    // addEdge(&g, 1, 3);
    // addEdge(&g, 1, 4);
    // addEdge(&g, 2, 5);
    // addEdge(&g, 2, 6);
    // addEdge(&g, 3, 7);
    // addEdge(&g, 3, 8);
    // addEdge(&g, 4, 9);
    // addEdge(&g, 5, 10);
    // addEdge(&g, 5, 11);
    // addEdge(&g, 6, 12);
    // addEdge(&g, 6, 13);
    // addEdge(&g, 7, 14);
    // addEdge(&g, 7, 15);
    // addEdge(&g, 8, 16);
    // addEdge(&g, 8, 17);
    // addEdge(&g, 9, 18);
    // addEdge(&g, 9, 19);

    for (int i = 0; i < 1000; ++i) {
    addEdge(&g, i, (i * 2 + 1) % 2000);
    addEdge(&g, i, (i * 2 + 2) % 2000);
}

    printf("Parcours DFS à partir du sommet 0 : ");
    DFS(&g, 0); // Perform DFS starting from vertex 0
    printf("\n");


    // Libérer la mémoire allouée pour les listes d'adjacence
    for (int i = 0; i < g.V; ++i)
        free(g.adj[i]);

    return 0;
}
