import java.util.*;

// Classe pour représenter un graphe non dirigé par une liste d'adjacence
class Graph {
    int V; // Nombre de sommets
    List<Integer>[] adj; // Liste d'adjacence

    @SuppressWarnings("unchecked")
    Graph(int V) {
        this.V = V;
        adj = new ArrayList[V];
        for (int i = 0; i < V; ++i)
            adj[i] = new ArrayList<>();
    }

    // Ajouter une arête au graphe
    void addEdge(int u, int v) {
        adj[u].add(v);
        adj[v].add(u); // Pour un graphe non dirigé
    }

    // Parcours en profondeur à partir d'un sommet donné
    void DFSUtil(int start, boolean[] visited, LinkedList<Integer> path) {
        visited[start] = true; // Marquer le sommet de départ comme visité
        path.add(start); // Ajouter le sommet de départ au chemin parcouru

        // Parcourir tous les sommets adjacents du sommet actuel
        for (int neighbor : adj[start]) {
            if (!visited[neighbor]) {
                DFSUtil(neighbor, visited, path); // Récursivement parcourir le voisin non visité
            }
        }
    }

    void DFS(int start) {
        boolean[] visited = new boolean[V]; // Tableau pour suivre les sommets visités
        LinkedList<Integer> path = new LinkedList<>(); // Liste chaînée pour stocker le chemin parcouru

        DFSUtil(start, visited, path); // Appel récursif de la fonction utilitaire DFS

        // Afficher le chemin parcouru
        System.out.print("Chemin parcouru : ");
        for (int node : path) {
            System.out.print(node + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        // Création d'un graphe non dirigé avec 20 sommets
        Graph g = new Graph(2000);
        // Ajout d'arêtes au graphe
        // g.addEdge(0, 1);
        // g.addEdge(0, 2);
        // g.addEdge(1, 3);
        // g.addEdge(1, 4);
        // g.addEdge(2, 5);
        // g.addEdge(2, 6);
        // g.addEdge(3, 7);
        // g.addEdge(3, 8);
        // g.addEdge(4, 9);
        // g.addEdge(5, 10);
        // g.addEdge(5, 11);
        // g.addEdge(6, 12);
        // g.addEdge(6, 13);
        // g.addEdge(7, 14);
        // g.addEdge(7, 15);
        // g.addEdge(8, 16);
        // g.addEdge(8, 17);
        // g.addEdge(9, 18);
        // g.addEdge(9, 19);

           for (int i = 0; i < 1000; ++i) {
            g.addEdge(i, (i * 2 + 1) % 2000);
            g.addEdge(i, (i * 2 + 2) % 2000);
        }

        System.out.print("Parcours DFS à partir du sommet 0 : ");
        g.DFS(0); // Parcours DFS à partir du sommet 0
    }
}
