public class Tree {
    static class TreeNode {
        TreeNode left_Node, right_Node;
    }

    // Crée un arbre binaire de profondeur spécifiée
    static TreeNode createTree(int treeDepth) {
        if (treeDepth <= 0)
            return null;

        TreeNode root = new TreeNode();
        root.left_Node = createTree(treeDepth - 1);
        root.right_Node = createTree(treeDepth - 1);

        return root;
    }

    // Calcule la somme de contrôle de l'arbre binaire
    static int computeTreeChecksum(TreeNode root) {
        if (root == null)
            return 0;

        return computeTreeChecksum(root.left_Node) +
               computeTreeChecksum(root.right_Node) + 1;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage: java BinaryTree <maximum_tree_depth>");
            return;
        }

        final int minimum_Tree_Depth = 4;
        int maximum_Tree_Depth = Math.max(Integer.parseInt(args[0]), minimum_Tree_Depth + 2);

        // Crée un arbre d'étirement de profondeur maximum_Tree_Depth+1
        TreeNode stretch_Tree = createTree(maximum_Tree_Depth + 1);
        System.out.println("Stretch tree of depth " + (maximum_Tree_Depth + 1) +
                           "\t checksum: " + computeTreeChecksum(stretch_Tree));

        // Crée l'arbre long_Lived_Tree de profondeur maximum_Tree_Depth
        TreeNode long_Lived_Tree = createTree(maximum_Tree_Depth);
        int long_Lived_Tree_Checksum = computeTreeChecksum(long_Lived_Tree);
        System.out.println("Long lived tree of depth " + maximum_Tree_Depth +
                           "\t checksum: " + long_Lived_Tree_Checksum);

        // Crée d'autres arbres binaires de différentes profondeurs
        for (int tree_Depth = minimum_Tree_Depth; tree_Depth <= maximum_Tree_Depth;
             tree_Depth += 2) {
            int total_Trees_Checksum = 0;
            int iterations = 1 << (maximum_Tree_Depth - tree_Depth + minimum_Tree_Depth);
            for (; iterations > 0; iterations--) {
                TreeNode tree = createTree(tree_Depth);
                total_Trees_Checksum += computeTreeChecksum(tree);
            }
            System.out.println((1 << (maximum_Tree_Depth - tree_Depth + minimum_Tree_Depth)) +
                               "\t trees of depth " + tree_Depth +
                               "\t checksum: " + total_Trees_Checksum);
        }
    }
}
