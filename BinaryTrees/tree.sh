#!/bin/bash

# Name of the files to store results
# TIME_FILE="time_results/time_results_python.txt"
# POWER_FILE="power_results/power_results_python.txt"

# # Clean any existing result files
# > $TIME_FILE
# > $POWER_FILE

# # Python implementation
# for i in {1..5}
# do
#     # Measure the time using built-in time and append to time_results.txt
#     (time python3 Tree.py 25) 2>> $TIME_FILE

# done

#for i in {1..2}
# do
#     python3 Tree.py 25 &
#     PYTHON_PID=$!
#     powerjoular -p $PYTHON_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 180
#     kill -INT $POWERJOULAR_PID
#     kill $PYTHON_PID

#done


############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_c.txt"
POWER_FILE="power_results/power_results_c.txt"

# Clean any existing result files
> $TIME_FILE
> $POWER_FILE

# C implementation
 gcc 2.c -o treeC
 for i in {1..3}
 do
     (time ./treeC 25) 2>> $TIME_FILE
 done

for i in {1..3}
do
    ./treeC 25 &
    PROGRAM_PID=$!
    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
    POWERJOULAR_PID=$!
    sleep 180
    kill -INT $POWERJOULAR_PID
    kill $PROGRAM_PID
done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cO2.txt"
POWER_FILE="power_results/power_results_cO2.txt"

# Clean any existing result files
> $TIME_FILE
> $POWER_FILE

# C implementation
gcc -o2 2.c -o treeC
 for i in {1..3}
 do
     (time ./treeC 25) 2>> $TIME_FILE
 done

for i in {1..3}
do
    ./treeC 25 &
    PROGRAM_PID=$!
    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
    POWERJOULAR_PID=$!
    sleep 180
    kill -INT $POWERJOULAR_PID
    kill $PROGRAM_PID
done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cO3.txt"
#POWER_FILE="power_results/power_results_cO3.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C implementation
gcc -o3 2.c -o treeC
 for i in {1..3}
 do
     (time ./treeC 25) 2>> $TIME_FILE
 done

for i in {1..3}
do
    ./treeC 25 &
    PROGRAM_PID=$!
    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
    POWERJOULAR_PID=$!
    sleep 180
    kill -INT $POWERJOULAR_PID
    kill $PROGRAM_PID
done

exit

# j'ai modifé power pour langage C mais ??? cpp > *3

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cpp.txt"
POWER_FILE="power_results/power_results_cpp.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C++ implementation
g++ Tree.cpp -o treeCpp

for i in {1..3}
do
    (time ./treeCpp 25) 2>> $TIME_FILE
done

for i in {1..3}
do
    ./treeCpp 25 &
    PROGRAM_PID=$!
    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
    POWERJOULAR_PID=$!
    sleep 180
    kill -INT $POWERJOULAR_PID
    kill $PROGRAM_PID
done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cppO3.txt"
POWER_FILE="power_results/power_results_cppO3.txt"

# Clean any existing result files
> $TIME_FILE
> $POWER_FILE

# C++ implementation
g++ -o3 Tree.cpp -o treeCpp

for i in {1..3}
do
    (time ./treeCpp 25) 2>> $TIME_FILE
done

for i in {1..3}
do
    ./treeCpp 25 &
    PROGRAM_PID=$!
    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
    POWERJOULAR_PID=$!
    sleep 180
    kill -INT $POWERJOULAR_PID
    kill $PROGRAM_PID
done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cpp.txt"
POWER_FILE="power_results/power_results_cpp.txt"

# Clean any existing result files
> $TIME_FILE
> $POWER_FILE

# C++ implementation
g++ Tree.cpp -o treeCpp

for i in {1..3}
do
    (time ./treeCpp 25) 2>> $TIME_FILE
done

# for i in {1..2}
# do
#     ./treeCpp 25 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_java.txt"
POWER_FILE="power_results_java.txt"

# Clean any existing result files
> $TIME_FILE
> $POWER_FILE

# Java implementation
javac Tree.java

for i in {1..3}
do

    (time java Tree 25) 2>> $TIME_FILE
done

for i in {1..3}
do
    java Tree 25 &
    PROGRAM_PID=$!
    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
    POWERJOULAR_PID=$!
    sleep 180
    kill -INT $POWERJOULAR_PID
    kill $PROGRAM_PID
done


############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_ruby.txt"
#POWER_FILE="power_results/power_results_ruby.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# Ruby implementation
for i in {1..2}
do

    (time ruby tree.rb 25) 2>> $TIME_FILE
done

# for i in {1..2}
# do
#     ruby tree.rb 25 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done
