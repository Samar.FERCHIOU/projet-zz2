#include <iostream>
#include <vector>
#include <memory>
#include <cstdlib>

struct TreeNode {
    std::shared_ptr<TreeNode> left_Node;
    std::shared_ptr<TreeNode> right_Node;
};

// Crée un arbre binaire de profondeur spécifiée
std::shared_ptr<TreeNode> create_Tree(int tree_Depth) {
    if (tree_Depth <= 0)
        return nullptr;

    auto root_Node = std::make_shared<TreeNode>();
    root_Node->left_Node = create_Tree(tree_Depth - 1);
    root_Node->right_Node = create_Tree(tree_Depth - 1);

    return root_Node;
}

// Calcule la somme de contrôle de l'arbre binaire
int compute_Tree_Checksum(const std::shared_ptr<TreeNode>& root_Node) {
    if (!root_Node)
        return 0;

    return compute_Tree_Checksum(root_Node->left_Node) +
           compute_Tree_Checksum(root_Node->right_Node) + 1;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " <maximum_tree_depth>" << std::endl;
        return 1;
    }

    const int minimum_Tree_Depth = 4;
    int maximum_Tree_Depth = std::max(std::atoi(argv[1]), minimum_Tree_Depth + 2);

    // Crée un arbre d'étirement de profondeur maximum_Tree_Depth+1
    auto stretch_Tree = create_Tree(maximum_Tree_Depth + 1);
    std::cout << "Stretch tree of depth " << (maximum_Tree_Depth + 1)
              << "\t checksum: " << compute_Tree_Checksum(stretch_Tree) << std::endl;

    // Crée l'arbre long_Lived_Tree de profondeur maximum_Tree_Depth
    auto long_Lived_Tree = create_Tree(maximum_Tree_Depth);
    int long_Lived_Tree_Checksum = compute_Tree_Checksum(long_Lived_Tree);
    std::cout << "Long lived tree of depth " << maximum_Tree_Depth
              << "\t checksum: " << long_Lived_Tree_Checksum << std::endl;

    // Crée d'autres arbres binaires de différentes profondeurs
    for (int tree_Depth = minimum_Tree_Depth; tree_Depth <= maximum_Tree_Depth;
         tree_Depth += 2) {
        int total_Trees_Checksum = 0;
        int iterations = 1 << (maximum_Tree_Depth - tree_Depth + minimum_Tree_Depth);
        for (; iterations > 0; iterations--) {
            auto tree = create_Tree(tree_Depth);
            total_Trees_Checksum += compute_Tree_Checksum(tree);
        }
        std::cout << (1 << (maximum_Tree_Depth - tree_Depth + minimum_Tree_Depth))
                  << "\t trees of depth " << tree_Depth
                  << "\t checksum: " << total_Trees_Checksum << std::endl;
    }

    return 0;
}
