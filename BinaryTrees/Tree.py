class TreeNode:
    def __init__(self):
        self.left_Node = None
        self.right_Node = None

# Crée un arbre binaire de profondeur spécifiée
def create_Tree(tree_Depth):
    if tree_Depth <= 0:
        return None
    
    root_Node = TreeNode()
    root_Node.left_Node = create_Tree(tree_Depth - 1)
    root_Node.right_Node = create_Tree(tree_Depth - 1)
    
    return root_Node

# Calcule la somme de contrôle de l'arbre binaire
def compute_Tree_Checksum(root_Node):
    if root_Node is None:
        return 0
    
    return compute_Tree_Checksum(root_Node.left_Node) + compute_Tree_Checksum(root_Node.right_Node) + 1

if __name__ == "__main__":
    import sys
    
    if len(sys.argv) != 2:
        print("Usage: python3 binary_trees.py <maximum_tree_depth>")
        sys.exit(1)
    
    minimum_Tree_Depth = 4
    maximum_Tree_Depth = max(int(sys.argv[1]), minimum_Tree_Depth + 2)
    
    # Crée un arbre d'étirement de profondeur maximum_Tree_Depth+1
    stretch_Tree = create_Tree(maximum_Tree_Depth + 1)
    print("Stretch tree of depth", maximum_Tree_Depth + 1, "checksum:", compute_Tree_Checksum(stretch_Tree))
    
    # Crée l'arbre long_Lived_Tree de profondeur maximum_Tree_Depth
    long_Lived_Tree = create_Tree(maximum_Tree_Depth)
    long_Lived_Tree_Checksum = compute_Tree_Checksum(long_Lived_Tree)
    print("Long lived tree of depth", maximum_Tree_Depth, "checksum:", long_Lived_Tree_Checksum)
    
    # Crée d'autres arbres binaires de différentes profondeurs
    for tree_Depth in range(minimum_Tree_Depth, maximum_Tree_Depth + 1, 2):
        total_Trees_Checksum = 0
        iterations = 1 << (maximum_Tree_Depth - tree_Depth + minimum_Tree_Depth)
        for _ in range(iterations):
            tree = create_Tree(tree_Depth)
            total_Trees_Checksum += compute_Tree_Checksum(tree)
        
        print(2**(maximum_Tree_Depth - tree_Depth + minimum_Tree_Depth), "trees of depth", tree_Depth,
              "checksum:", total_Trees_Checksum)
