class TreeNode
    attr_accessor :left_Node, :right_Node
    
    def initialize
      @left_Node = nil
      @right_Node = nil
    end
  end
  
  # Crée un arbre binaire de profondeur spécifiée
  def create_Tree(tree_Depth)
    return nil if tree_Depth <= 0
    
    root_Node = TreeNode.new
    root_Node.left_Node = create_Tree(tree_Depth - 1)
    root_Node.right_Node = create_Tree(tree_Depth - 1)
    
    return root_Node
  end
  
  # Calcule la somme de contrôle de l'arbre binaire
  def compute_Tree_Checksum(root_Node)
    return 0 if root_Node.nil?
    
    return compute_Tree_Checksum(root_Node.left_Node) +
           compute_Tree_Checksum(root_Node.right_Node) + 1
  end
  
  if __FILE__ == $PROGRAM_NAME
    if ARGV.length != 1
      puts "Usage: ruby binary_trees.rb <maximum_tree_depth>"
      exit(1)
    end
    
    minimum_Tree_Depth = 4
    maximum_Tree_Depth = [ARGV[0].to_i, minimum_Tree_Depth + 2].max
    
    # Crée un arbre d'étirement de profondeur maximum_Tree_Depth+1
    stretch_Tree = create_Tree(maximum_Tree_Depth + 1)
    puts "Stretch tree of depth #{maximum_Tree_Depth + 1}\t checksum: #{compute_Tree_Checksum(stretch_Tree)}"
    
    # Crée l'arbre long_Lived_Tree de profondeur maximum_Tree_Depth
    long_Lived_Tree = create_Tree(maximum_Tree_Depth)
    long_Lived_Tree_Checksum = compute_Tree_Checksum(long_Lived_Tree)
    puts "Long lived tree of depth #{maximum_Tree_Depth}\t checksum: #{long_Lived_Tree_Checksum}"
    
    # Crée d'autres arbres binaires de différentes profondeurs
    (minimum_Tree_Depth..maximum_Tree_Depth).step(2) do |tree_Depth|
      total_Trees_Checksum = 0
      iterations = 1 << (maximum_Tree_Depth - tree_Depth + minimum_Tree_Depth)
      iterations.times do
        tree = create_Tree(tree_Depth)
        total_Trees_Checksum += compute_Tree_Checksum(tree)
      end
      puts "#{2**(maximum_Tree_Depth - tree_Depth + minimum_Tree_Depth)}\t trees of depth #{tree_Depth}\t checksum: #{total_Trees_Checksum}"
    end
  end
  