TIME_FILE="time_results/time_results_c_nouv.txt"
#POWER_FILE="power_results/power_results_c.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C implementation
 gcc 2.c -o treeC
 for i in {1..3}
 do
     (time ./treeC 25) 2>> $TIME_FILE
 done

###########################
TIME_FILE="time_results/time_results_co2_nouv.txt"
#POWER_FILE="power_results/power_results_c.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C implementation
 gcc -O2 2.c -o treeC2
 for i in {1..3}
 do
     (time ./treeC2 25) 2>> $TIME_FILE
 done

##########################

TIME_FILE="time_results/time_results_co3_nouv.txt"
#POWER_FILE="power_results/power_results_c.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C implementation
 gcc -O3 2.c -o treeC3
 for i in {1..3}
 do
     (time ./treeC3 25) 2>> $TIME_FILE
 done
