#include <stdio.h>
#include <stdlib.h>

typedef struct tree_node {
  struct tree_node *left_Node, *right_Node;
} tree_node;

// Crée un arbre binaire de profondeur spécifiée
static tree_node *create_Tree(const int tree_Depth) {
  if (tree_Depth <= 0)
    return NULL;

  tree_node *root_Node = (tree_node *)malloc(sizeof(tree_node));
  root_Node->left_Node = create_Tree(tree_Depth - 1);
  root_Node->right_Node = create_Tree(tree_Depth - 1);

  return root_Node;
}

// Calcule la somme de contrôle de l'arbre binaire
static int compute_Tree_Checksum(const tree_node *const root_Node) {
  if (root_Node == NULL)
    return 0;

  return compute_Tree_Checksum(root_Node->left_Node) +
         compute_Tree_Checksum(root_Node->right_Node) + 1;
}

int main(int argc, char *argv[]) {
  if (argc != 2) {
    printf("Usage: %s <maximum_tree_depth>\n", argv[0]);
    return 1;
  }

  const int minimum_Tree_Depth = 4;
  const int maximum_Tree_Depth = atoi(argv[1]) < minimum_Tree_Depth + 2
                                      ? minimum_Tree_Depth + 2
                                      : atoi(argv[1]);

  // Crée un arbre d'étirement de profondeur maximum_Tree_Depth+1
  tree_node *stretch_Tree = create_Tree(maximum_Tree_Depth + 1);
  printf("Stretch tree of depth %d\t checksum: %d\n",
         maximum_Tree_Depth + 1, compute_Tree_Checksum(stretch_Tree));
  free(stretch_Tree);

  // Crée l'arbre long_Lived_Tree de profondeur maximum_Tree_Depth
  tree_node *long_Lived_Tree = create_Tree(maximum_Tree_Depth);
  int long_Lived_Tree_Checksum = compute_Tree_Checksum(long_Lived_Tree);
  printf("Long lived tree of depth %d\t checksum: %d\n",
         maximum_Tree_Depth, long_Lived_Tree_Checksum);

  // Crée d'autres arbres binaires de différentes profondeurs
  for (int tree_Depth = minimum_Tree_Depth; tree_Depth <= maximum_Tree_Depth;
       tree_Depth += 2) {
    int total_Trees_Checksum = 0;
    int iterations = 1 << (maximum_Tree_Depth - tree_Depth + minimum_Tree_Depth);
    for (; iterations > 0; iterations--) {
      tree_node *tree = create_Tree(tree_Depth);
      total_Trees_Checksum += compute_Tree_Checksum(tree);
      free(tree);
    }
    printf("%d\t trees of depth %d\t checksum: %d\n",
           1 << (maximum_Tree_Depth - tree_Depth + minimum_Tree_Depth),
           tree_Depth, total_Trees_Checksum);
  }

  free(long_Lived_Tree);
  return 0;
}
