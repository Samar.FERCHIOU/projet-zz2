import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

data = {
    'langage': ['Python', 'C', 'C -o2', 'C -o3', 'Cpp', 'cppO2', 'cppO3', 'java', 'ruby'],
    'real': [673.736083333333, 73.4400833333333, 73.4933333333333, 73.4964, 194.6645, 194.5976, 194.6022, 36.68575, 1279.21],

    'user': [673.676416666667, 73.4334166666667, 73.4863333333333, 73.4906, 194.649166666667, 194.5816, 194.5892, 36.73, 1276.512875]


}


df = pd.DataFrame(data)


# Statistiques descriptives
print(df.describe())

# Visualisation des temps d'exécution
sns.boxplot(x='langage', y='real', data=df)
plt.show()

sns.boxplot(x='langage', y='user', data=df)
plt.show()
