public class Gen {
    public static boolean isPrime(int num) {
        if (num <= 1)
            return false;
        for (int i = 2; i <= Math.sqrt(num); ++i) {
            if (num % i == 0)
                return false;
        }
        return true;
    }

    public static void generatePrimes(int n) {
        int count = 0;
        int currentNumber = 2;
        System.out.print("First " + n + " prime numbers: ");
        while (count < n) {
            if (isPrime(currentNumber)) {
                System.out.print(currentNumber + " ");
                ++count;
            }
            ++currentNumber;
        }
        System.out.println();
    }

    public static void main(String[] args) {
        generatePrimes(1000000);
    }
}
