#include <stdio.h>
#include <math.h>

int is_prime(int num) {
    if (num <= 1)
        return 0;
    for (int i = 2; i <= sqrt(num); ++i) {
        if (num % i == 0)
            return 0;
    }
    return 1;
}

void generate_primes(int n) {
    int count = 0;
    int current_number = 2;
    printf("First %d prime numbers: ", n);
    while (count < n) {
        if (is_prime(current_number)) {
            printf("%d ", current_number);
            ++count;
        }
        ++current_number;
    }
    printf("\n");
}

int main() {
    generate_primes(1000000);
    return 0;
}
