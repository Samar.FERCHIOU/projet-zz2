def generate_primes(n)
    primes = []
    current_number = 2
    while primes.length < n do
        is_prime = true
        (2..Math.sqrt(current_number)).each do |i|
            if current_number % i == 0
                is_prime = false
                break
            end
        end
        primes << current_number if is_prime
        current_number += 1
    end
    primes
end

# Générer les 1000 premiers nombres premiers
primes = generate_primes(1000000)
puts "First 1000 prime numbers: #{primes.join(', ')}"
