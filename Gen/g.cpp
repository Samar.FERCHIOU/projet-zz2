#include <iostream>
#include <cmath>

bool is_prime(int num) {
    if (num <= 1)
        return false;
    for (int i = 2; i <= std::sqrt(num); ++i) {
        if (num % i == 0)
            return false;
    }
    return true;
}

void generate_primes(int n) {
    int count = 0;
    int current_number = 2;
    std::cout << "First " << n << " prime numbers: ";
    while (count < n) {
        if (is_prime(current_number)) {
            std::cout << current_number << " ";
            ++count;
        }
        ++current_number;
    }
    std::cout << std::endl;
}

int main() {
    generate_primes(1000000);
    return 0;
}
