# #!/bin/bash

# ############################################
# ############################################
# ############################################

# # # Name of the files to store results
# TIME_FILE="time_results/time_results_python.txt"
POWER_FILE="power_results/power_results_python.txt"

# # # Clean any existing result files
#  > $TIME_FILE
#> $POWER_FILE

# # # Python implementation
#   for i in {1..6}
#   do
# #      # Measure the time using built-in time and append to time_results.txt
#       (time python3 g.py) 2>> $TIME_FILE

#   done


for i in {1..5}
do
    # powerjoular measures the power consumption while running your_python_script.py
    # Append the result to power_results.txt
   python3 g.py &
   PYTHON_PID=$!
   powerjoular -p $PYTHON_PID >> $POWER_FILE &
   POWERJOULAR_PID=$!
   sleep 140
   kill -INT $POWERJOULAR_PID
   kill $PYTHON_PID

done


# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_c.txt"
# POWER_FILE="power_results/power_results_c.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation
#  gcc g.c -o fC -lm
# #for i in {1..8}
# # do
# #     (time ./fC )2>> $TIME_FILE
# # done

#  for i in {1..8}
#  do
#      ./fC &
#      PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 10
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_cO2.txt"
# POWER_FILE="power_results/power_results_cO2.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C o2 implementation
#   gcc -o2 g.c -o fCo2 -lm
# #  for i in {1..8}
# #  do
# #      (time ./fCo2 )2>> $TIME_FILE
# #  done

# for i in {1..8}
# do
#     ./fCo2 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 10
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_cO3.txt"
# POWER_FILE="power_results/power_results_cO3.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C o3 implementation
#   gcc -o3 g.c -o fCo3 -lm
# #  for i in {1..8}
# #  do
# #      (time ./fCo3 )2>> $TIME_FILE
# #  done

# for i in {1..8}
# do
#     ./fCo3 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 10
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done


# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_cpp.txt"
# POWER_FILE="power_results/power_results_cpp.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # # C++ implementation
# g++ g.cpp -o fCpp

# #  for i in {1..8}
# #  do
# #      (time ./fCpp) 2>> $TIME_FILE
# #  done

# for i in {1..6}
# do
#     ./fCpp &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 14
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# # TIME_FILE="time_results/time_results_cppO2.txt"
#  POWER_FILE="power_results/power_results_cppO2.txt"

# # Clean any existing result files
# # > $TIME_FILE
# > $POWER_FILE

# # C++ implementation o2
# g++ -o2 g.cpp -o fCppO2

# # for i in {1..6}
# # do
# #     (time ./fCppO2) 2>> $TIME_FILE
# # done

# for i in {1..8}
# do
#     ./fCppO2 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 14
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# # TIME_FILE="time_results/time_results_cppO3.txt"
# POWER_FILE="power_results/power_results_cppO3.txt"

# #Clean any existing result files
# # > $TIME_FILE
# > $POWER_FILE

# # C++ implementation o2
# g++ -o3 g.cpp -o fCppO3

# #  for i in {1..6}
# #  do
# #      (time ./fCppO3) 2>> $TIME_FILE
# #  done

# for i in {1..8}
# do
#     ./fCppO3 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 14
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done


# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# # TIME_FILE="time_results/time_results_java.txt"
# POWER_FILE="power_results/power_results_java.txt"

# # Clean any existing result files
# # > $TIME_FILE
# > $POWER_FILE

# # # Java implementation
#  javac Gen.java

# # for i in {1..6}
# # do
# #     (time java Gen) 2>> $TIME_FILE
# # done

# for i in {1..8}
# do
#     java Gen &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 10
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done


# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_ruby.txt"
# POWER_FILE="power_results/power_results_ruby.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # Ruby implementation
# # for i in {1..6}
# # do

# #     (time ruby g.rb) 2>> $TIME_FILE
# # done

# for i in {1..8}
# do
#     ruby g.rb &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 365
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done
