import os
import re
import numpy as np
import matplotlib.pyplot as plt

# def extract_time(time_str):
#     match = re.match(r"(\w+)\t(\d+m)?(\d+),(\d+)s", time_str)
#     if match:
#         time_type = match.group(1)
#         minutes = int(match.group(2)[:-1]) 
#         seconds = float(match.group(3) + '.' + match.group(4))
#         return minutes * 60 + seconds
#     else:
#         return 0
    

def extract_total_energy(line):
    match = re.search(r'Total energy: (\d+\.\d+) Joules', line)
    if match:
        return float(match.group(1))
    return None

def process_file(input_file, output_file):
    total_energies = []

    with open(input_file, 'r') as file:
        lines = file.readlines()

    current_energy_block = None
    for line in lines:
        if line.startswith('Total energy:'):
            current_energy_block = extract_total_energy(line)
        elif line.strip() == '' and current_energy_block is not None:
            total_energies.append(current_energy_block)
            current_energy_block = None

    if current_energy_block is not None:
        total_energies.append(current_energy_block)

    if total_energies:
        average_energy = sum(total_energies) / len(total_energies)
        with open(output_file, 'a') as out_file:
          #  out_file.write(f'Total Energies: {total_energies}\n')
            out_file.write(f'{input_file}:\nAverage Energy: {average_energy} Joules\n\n')

# # Function to read and process data from a file
# def process_file(filename, output_file ):
#     real_times = []
#     user_times = []
#     sys_times = []

#     with open(filename, 'r') as file:
#         lines = file.readlines()
#         for line in lines:
#             if line.startswith('real'):
#                 real_times.append(extract_time(line))
#             elif line.startswith('user'):
#                 user_times.append(extract_time(line))
#             elif line.startswith('sys'):
#                 sys_times.append(extract_time(line))
    
#     with open(output_file, 'a') as out_file:
#         out_file.write(f"{filename}:\nreal: {np.mean(real_times)}, user: {np.mean(user_times)}, sys: {np.mean(sys_times)}\n\n")

#     #print(filename, " : real : ",np.mean(real_times), " user : ", np.mean(user_times), " sys : ", np.mean(sys_times))
#     return np.mean(real_times), np.mean(user_times), np.mean(sys_times)

# # Function to plot the results
# def plot_results(real_means, user_means, sys_means, filenames, output_fig):
#     plt.figure(figsize=(10, 6))
#     plt.plot(filenames, real_means, marker='o', label='Real Time')
#     plt.plot(filenames, user_means, marker='o', label='User Time')
#     plt.plot(filenames, sys_means, marker='o', label='Sys Time')
#     plt.title('simPi')
#     plt.xlabel('File')
#     plt.ylabel('Average Time (s)')
#     plt.xticks(rotation=45)
#     plt.legend()
#     plt.tight_layout()
#     plt.savefig(output_fig)  
#     plt.show()

 

# Main function
def main():
    directory = "power_results"
    files = os.listdir(directory)

   # output_fig = "time.png"

    output_file = "power_results/avg.txt" 
    if os.path.exists(output_file):
        os.remove(output_file)

    for file in files:
        if file.endswith(".txt"):
            process_file(os.path.join(directory, file),  output_file )
   # plot_results(real_means, user_means, sys_means, files, output_fig)

if __name__ == "__main__":
    main()
