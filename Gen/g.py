import math

def is_prime(num):
    if num <= 1:
        return False
    for i in range(2, int(math.sqrt(num)) + 1):
        if num % i == 0:
            return False
    return True

def generate_primes(n):
    count = 0
    current_number = 2
    print("First", n, "prime numbers:", end=" ")
    while count < n:
        if is_prime(current_number):
            print(current_number, end=" ")
            count += 1
        current_number += 1
    print()

generate_primes(1000000)

