# #!/bin/bash

# #rm -f results/matrix_c.txt
# #rm -f results/matrix_cpp.txt
# #rm -f results/matrix_java.txt
# #rm -f results/matrix_py.txt
# #rm -f results/matrix_ruby.txt

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# TIME_FILE="time_results/time_results_python.txt"
# #POWER_FILE="power_results_python.txt"

# # Clean any existing result files
# > $TIME_FILE
# #> $POWER_FILE

# # Python implementation
# for i in {1..5}
# do
#     # Measure the time using built-in time and append to time_results.txt
#     (time python3 Cal.py) 2>> $TIME_FILE

# done

# #for i in {1..30}
# # do
# #     # powerjoular measures the power consumption while running your_python_script.py
# #     # Append the result to power_results.txt
# #     python3 your_python_script.py &
# #     PYTHON_PID=$!
# #     powerjoular -p $PYTHON_PID >> $POWER_FILE_PYTHON &
# #     POWERJOULAR_PID=$!
# #     sleep 4
# #     kill -INT $POWERJOULAR_PID
# #     wait $PYTHON_PID

# # done


# ############################################
# ############################################
# ############################################

# # # Name of the files to store results
# # TIME_FILE="time_results/time_results_c.txt"
# POWER_FILE="power_results/power_results_c.txt"

# # # # Clean any existing result files
# # # > $TIME_FILE
# > $POWER_FILE

# # # # C implementation
# gcc Cal.c -o calC
# # # for i in {1..10}
# # # do
# # #     (time ./calC) 2>> $TIME_FILE
# # # done

# for i in {1..10}
# do
#     ./calC &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 30
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done


# ############################################
# ############################################
# ############################################

# #Name of the files to store results
# #TIME_FILE="time_results/time_results_cO2.txt"
# POWER_FILE="power_results/power_results_cO2.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation
# gcc -o2 Cal.c -o calCo2 
# # for i in {1..10}
# # do
# #     (time ./calCo2) 2>> $TIME_FILE
# # done

# for i in {1..10}
# do
#     ./calCo2 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 30
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done
# >>>>>>> 475217e042678d9031c02c87a4e5dd4baf909d5d


# ############################################
# ############################################
# ############################################


# #Name of the files to store results
# #TIME_FILE="time_results/time_results_cO3.txt"
# POWER_FILE="power_results/power_results_cO3.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation
# gcc -o3  Cal.c -o calCo3 
# # for i in {1..10}
# # do
# #     (time ./calCo3) 2>> $TIME_FILE
# # done

# for i in {1..10}
# do
#     ./calCo3 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 30
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done



# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# # TIME_FILE="time_results/time_results_cpp.txt"
# POWER_FILE="power_results/power_results_cpp.txt"

# # # Clean any existing result files
# # > $TIME_FILE
# > $POWER_FILE

# # # C++ implementation
# g++ Cal.cpp -o calCpp

# # for i in {1..10}
# # do
# #     (time ./calCpp) 2>> $TIME_FILE
# # done

# for i in {1..10}
# do
#     ./calCpp &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 30
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done




# #Name of the files to store results
# #TIME_FILE="time_results/time_results_cppO2.txt"
# POWER_FILE="power_results/power_results_cppO2.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C++ implementation
# g++ -o2  Cal.cpp -o calCppo2 
# # for i in {1..10}
# # do
# #     (time ./calCppo2) 2>> $TIME_FILE
# # done

# for i in {1..10}
# do
#     ./calCppo2 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 30
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done



# ############################################
# ############################################
# ############################################

# #Name of the files to store results
# #TIME_FILE="time_results/time_results_cppO3.txt"
# POWER_FILE="power_results/power_results_cppO3.txt"

# # Clean any existing result files
# #> $TIME_FILE

# > $POWER_FILE

# # C++ implementation
# g++ -o3 Cal.cpp -o calCppo3
# # for i in {1..10}
# # do
# #     (time ./calCppo3) 2>> $TIME_FILE
# # done

# for i in {1..10}
# do
#     ./calCppo3 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 30
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done


# ############################################
# ############################################
# ############################################

# # # Name of the files to store results
# # TIME_FILE="time_results/time_results_java.txt"
# POWER_FILE="power_results/power_results_java.txt"

# # # Clean any existing result files
# # > $TIME_FILE
# > $POWER_FILE

# # # Java implementation
# javac Cal.java

# # for i in {1..10}
# # do

# #     (time java Cal) 2>> $TIME_FILE
# # done

# for i in {1..10}
# do
#     java Cal &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 6.5
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done


# # ############################################
# # ############################################
# # ############################################

# # Name of the files to store results
# # TIME_FILE="time_results/time_results_ruby.txt"
# POWER_FILE="power_results/power_results_ruby.txt"

# # # Clean any existing result files
# # > $TIME_FILE
# > $POWER_FILE

# # # Ruby implementation
# # for i in {1..5}
# # do
# #     (time ruby Cal.rb) 2>> $TIME_FILE
# # done

# for i in {1..3}
# do
#     ruby Cal.rb &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 700
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

ORIGINAL_DIR=$(pwd)
# Name of the files to store results
TIME_FILE="$ORIGINAL_DIR/time_results/time_results_csharp.txt"
POWER_FILE="$ORIGINAL_DIR/power_results/power_results_csharp.txt"

# Clean any existing result files
#> $TIME_FILE
> $POWER_FILE

cd csharp/csharp

# Csharp implementation
/home/samar/.dotnet/dotnet build

#for i in {1..10}
#do
#   (time /home/samar/.dotnet/dotnet run) 2>> $TIME_FILE
#done

 for i in {1..5}
 do
     /home/samar/.dotnet/dotnet run &
     PROGRAM_PID=$!
     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
     POWERJOULAR_PID=$!
     sleep 50
     kill -INT $POWERJOULAR_PID
     kill  $PROGRAM_PID
 done

cd "$ORIGINAL_DIR"
