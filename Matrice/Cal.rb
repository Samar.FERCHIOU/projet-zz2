#SIZE = 1800

SIZE = 8

# Initialisation de la matrice A avec des valeurs croissantes en float avec deux chiffres après la virgule
valueA = 0.99
A = Array.new(SIZE) { Array.new(SIZE) { |i| valueA += 0.01 } }

# Initialisation de la matrice B avec des valeurs décroissantes en float avec deux chiffres après la virgule
valueB = 1.98
B = Array.new(SIZE) { Array.new(SIZE) { |i| valueB += 0.02 } }

# Calcul de la matrice C
C = Array.new(SIZE) { Array.new(SIZE, 0.0) }
for i in 0...SIZE
  for j in 0...SIZE
    for k in 0...SIZE
      C[i][j] += A[i][k] * B[k][j]
    end
  end
end

#Enregistrement de la matrice C dans un fichier texte
# File.open("results2/matrix_ruby.txt", "w") do |file|
#   C.each do |row|
#     file.puts(row.map { |val| "%.6f" % val }.join(" "))
#   end
#   file.puts("\n\n")
# end

