﻿class Program
{
    static void Main()
    {
        const int SIZE = 1800;
        float[][] A = new float[SIZE][];
        float[][] B = new float[SIZE][];
        float[][] C = new float[SIZE][];

        // Allocate memory for the arrays of pointers
        for (int i = 0; i < SIZE; ++i)
        {
            A[i] = new float[SIZE];
            B[i] = new float[SIZE];
            C[i] = new float[SIZE];
        }

        // Initialisation de la matrice A avec des valeurs croissantes en float avec deux chiffres après la virgule
        float valueA = 1.00f;
        for (int i = 0; i < SIZE; ++i)
        {
            for (int j = 0; j < SIZE; ++j)
            {
                A[i][j] = valueA;
                valueA += 0.01f; 
            }
        }

        // Initialisation de la matrice B avec des valeurs décroissantes en float avec deux chiffres après la virgule
        float valueB = 2.00f;
        for (int i = 0; i < SIZE; ++i)
        {
            for (int j = 0; j < SIZE; ++j)
            {
                B[i][j] = valueB;
                valueB += 0.02f; 
            }
        }

        // Calcul de la matrice C
        for (int i = 0; i < SIZE; ++i)
        {
            for (int j = 0; j < SIZE; ++j)
            {
                C[i][j] = 0.0f;
                for (int k = 0; k < SIZE; ++k)
                {
                    C[i][j] += A[i][k] * B[k][j];
                }
            }
        }

        
    }
}


