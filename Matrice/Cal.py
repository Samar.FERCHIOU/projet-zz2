import numpy as np

#SIZE = 1800

SIZE = 8
# Initialisation de la matrice A avec des valeurs croissantes en float avec deux chiffres après la virgule
valueA = 1.00
A = np.zeros((SIZE, SIZE))
for i in range(SIZE):
    for j in range(SIZE):
        A[i][j] = valueA
        valueA += 0.01  # Incrémentation de la valeur de la matrice A

# # Enregistrer la matrice C dans un fichier texte
# with open("matrix_Apy.txt", "a") as file_B:
#     for i in range(SIZE):
#         for j in range(SIZE):
#             #file_B.write(str(A[i][j]) + " ")
#             file_B.write("{:.6f} ".format(A[i][j]))
#         file_B.write("\n")
#     file_B.write("\n\n")

# Initialisation de la matrice B avec des valeurs décroissantes en float avec deux chiffres après la virgule
valueB = 2.00
B = np.zeros((SIZE, SIZE))
for i in range(SIZE):
    for j in range(SIZE):
        B[i][j] = valueB
        valueB += 0.02  # Décrémentation de la valeur de la matrice B

# # Calcul de la matrice 
#C = np.zeros((SIZE, SIZE))
#for i in range(SIZE):
#     for j in range(SIZE):
#         for k in range(SIZE):
#             C[i][j] += A[i][k] * B[k][j]

C = np.dot(A, B)

# Enregistrer la matrice C dans un fichier texte
# with open("results2/matrix_py.txt", "w") as file_A:
#     for i in range(SIZE):
#         for j in range(SIZE):
#             #file_A.write(str(C[i][j]) + " ")
#             file_A.write("{:.6f} ".format(C[i][j]))
#         file_A.write("\n")
#     file_A.write("\n\n")



