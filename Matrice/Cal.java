import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Cal {
    //private static final int SIZE = 1800;
    private static final int SIZE = 8;

    public static void main(String[] args) {
        double[][] A = new double[SIZE][SIZE];
        double[][] B = new double[SIZE][SIZE];
        double[][] C = new double[SIZE][SIZE];

        // Initialisation de la matrice A avec des valeurs croissantes en double avec deux chiffres après la virgule
        double valueA = 1.00f;
        for (int i = 0; i < SIZE; ++i) {
            for (int j = 0; j < SIZE; ++j) {
                A[i][j] = valueA;
                valueA += 0.01f; // Incrémentation de la valeur de la matrice A
            }
        }

        // Initialisation de la matrice B avec des valeurs décroissantes en double avec deux chiffres après la virgule
        double valueB = 2.00f;
        for (int i = 0; i < SIZE; ++i) {
            for (int j = 0; j < SIZE; ++j) {
                B[i][j] = valueB;
                valueB += 0.02f; // Décrémentation de la valeur de la matrice B
            }
        }

        // Calcul de la matrice C
        for (int i = 0; i < SIZE; ++i) {
            for (int j = 0; j < SIZE; ++j) {
                C[i][j] = 0.0f;
                for (int k = 0; k < SIZE; ++k) {
                    C[i][j] += A[i][k] * B[k][j];
                }
            }
        }

        // Enregistrer la matrice C dans un fichier texte
        // try {
        //     PrintWriter file_C = new PrintWriter(new FileWriter("results2/matrix_java.txt"));

        //     for (int i = 0; i < SIZE; ++i) {
        //         for (int j = 0; j < SIZE; ++j) {
        //              file_C.print(String.format("%.6f ", C[i][j]).replace(",", "."));
        //         }
        //         file_C.println();
        //     }

        //     file_C.println("\n\n");
        //     file_C.close();
        // } catch (IOException e) {
        //     System.err.println("Impossible d'écrire dans le fichier pour la matrice C.");
        //     e.printStackTrace();
        // }
    }
}


