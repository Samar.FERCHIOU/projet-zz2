#!/bin/bash

file1="matrix_c.txt"
file2="matrix_py.txt"

# Vérifier si les deux fichiers ont le même nombre de lignes
lines_file1=$(wc -l < "$file1")
lines_file2=$(wc -l < "$file2")

if [ "$lines_file1" -ne "$lines_file2" ]; then
    echo "Les fichiers n'ont pas le même nombre de lignes."
    exit 1
fi

# Comparer les fichiers ligne par ligne
while IFS= read -r line1 && IFS= read -r line2 <&3; do
    if [ "$line1" != "$line2" ]; then
        echo "Les fichiers sont différents."
        exit 1
    fi
done < "$file1" 3< "$file2"

echo "Les fichiers sont identiques."
