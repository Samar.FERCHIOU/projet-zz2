#include <iostream>
#include <fstream>
#include <iomanip> 


using namespace std;

//#define SIZE 1800

#define SIZE 8

int main() {
    // double A[SIZE][SIZE];
    // double B[SIZE][SIZE];
    // double C[SIZE][SIZE];
double **A, **B, **C;
    
    // Allocation dynamique de mémoire pour les tableaux A, B et C
    A = new double*[SIZE];
    B = new double*[SIZE];
    C = new double*[SIZE];

    for (int i = 0; i < SIZE; ++i) {
        A[i] = new double[SIZE];
        B[i] = new double[SIZE];
        C[i] = new double[SIZE];
    }
    // Initialisation de la matrice A avec des valeurs croissantes en double avec deux chiffres après la virgule
    double valueA = 1.00f;
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            A[i][j] = valueA;
            valueA += 0.01f; // Incrémentation de la valeur de la matrice A
        }
    }

    // Initialisation de la matrice B avec des valeurs décroissantes en double avec deux chiffres après la virgule
    double valueB = 2.00f;
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            B[i][j] = valueB;
            valueB += 0.02f; // Décrémentation de la valeur de la matrice B
        }
    }

    // Calcul de la matrice C
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            C[i][j] = 0.0f;
            for (int k = 0; k < SIZE; ++k) {
                C[i][j] += A[i][k] * B[k][j];
            }
        }
    }

    // Enregistrement des matrices dans des fichiers texte
    //ofstream file_A("matrix_Acpp.txt");
    //ofstream file_B("matrix_Bcpp.txt");
    //ofstream file_C("results2/matrix_cpp.txt");

    // //if (!file_A || !file_B || !file_C) {
    // if (!file_C) {
    //     cerr << "Impossible d'ouvrir les fichiers pour enregistrement des matrices." << endl;
    //     return 1;
    // }

    // // //file_A << fixed << setprecision(6); // Formatage des nombres flottants avec deux chiffres après la virgule
    // // //file_B << fixed << setprecision(6);
    //  file_C << fixed << setprecision(6);

    // for (int i = 0; i < SIZE; ++i) {
    //     for (int j = 0; j < SIZE; ++j) {
    //         //file_A << A[i][j] << " ";
    //         //file_B << B[i][j] << " ";
    //         file_C << C[i][j] << " ";
    //     }
    //     //file_A << endl;
    //     //file_B << endl;
    //     file_C << endl;
    // }

    // file_C << endl << endl;

    return 0;
}
