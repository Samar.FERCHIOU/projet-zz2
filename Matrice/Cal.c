#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SIZE 8

//#define SIZE 1800


int main(void)
{   
    // double A[SIZE][SIZE];
    // double B[SIZE][SIZE];
    // double C[SIZE][SIZE];
    
    double **A, **B, **C;
       
    // Allocate memory for the arrays of pointers
    A = (double **)malloc(SIZE * sizeof(double *));
    B = (double **)malloc(SIZE * sizeof(double *));
    C = (double **)malloc(SIZE * sizeof(double *));

    // Allocate memory for each row of the matrices
    for (int i = 0; i < SIZE; ++i) {
        A[i] = (double *)malloc(SIZE * sizeof(double));
        B[i] = (double *)malloc(SIZE * sizeof(double));
        C[i] = (double *)malloc(SIZE * sizeof(double));
    }
    // Initialisation de la matrice A avec des valeurs croissantes en double avec deux chiffres après la virgule
    double valueA = 1.00f;
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            A[i][j] = valueA;
            valueA += 0.01f; // Incrémentation de la valeur de la matrice A
        }
    }

    // Initialisation de la matrice B avec des valeurs décroissantes en double avec deux chiffres après la virgule
    double valueB = 2.00f;
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            B[i][j] = valueB;
            valueB += 0.02f; // Décrémentation de la valeur de la matrice B
        }
    }

    // Calcul de la matrice C
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            C[i][j] = 0.0f;
            for (int k = 0; k < SIZE; ++k) {
                C[i][j] += A[i][k] * B[k][j];
            }
        }
    }

    // // Enregistrer la matrice A dans un fichier texte
     //FILE *file_A = fopen("matrix_Ac.txt", "w");
    // if (file_A == NULL) {
    //     fprintf(stderr, "Impossible d'ouvrir le fichier pour la matrice A.\n");
    //     return 1;
    // }

    // for (int i = 0; i < SIZE; ++i) {
    //     for (int j = 0; j < SIZE; ++j) {
    //         fprintf(file_A, "%.6f ", A[i][j]); // Écrire chaque élément de la matrice avec 6 chiffres après la virgule
    //     }
    //     fprintf(file_A, "\n"); // Aller à la ligne pour chaque nouvelle rangée de la matrice
    // }

    // fclose(file_A); // Fermer le fichier après avoir fini d'écrire

    // // Enregistrer la matrice B dans un fichier texte
    // FILE *file_B = fopen("matrix_Bc.txt", "w");
    // if (file_B == NULL) {
    //     fprintf(stderr, "Impossible d'ouvrir le fichier pour la matrice B.\n");
    //     return 1;
    // }

    // for (int i = 0; i < SIZE; ++i) {
    //     for (int j = 0; j < SIZE; ++j) {
    //         fprintf(file_B, "%.6f ", B[i][j]);
    //     }
    //     fprintf(file_B, "\n");
    // }

    // fclose(file_B);

    // // Enregistrer la matrice C dans un fichier texte
    // FILE *file_C = fopen("results2/matrix_c.txt", "w");
    // if (file_C == NULL) {
    //     fprintf(stderr, "Impossible d'ouvrir le fichier pour la matrice B.\n");
    //     return 1;
    // }

    // for (int i = 0; i < SIZE; ++i) {
    //     for (int j = 0; j < SIZE; ++j) {
    //         fprintf(file_C, "%.6lf ", C[i][j]);
    //     }
    //     fprintf(file_C, "\n");
    // }

    // fprintf(file_C, "\n\n");
    // fclose(file_C);
    
    // Free allocated memory
    for (int i = 0; i < SIZE; ++i) {
        free(A[i]);
        free(B[i]);
        free(C[i]);
    }
    free(A);
    free(B);
    free(C);


    return 0;
}
