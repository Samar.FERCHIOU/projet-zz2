#!/bin/bash

# Name of the files to store results
#TIME_FILE="time_results/time_results_python.txt"
#POWER_FILE="power_results_python.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# Python implementation
#for i in {1..30}
#do
    # Measure the time using built-in time and append to time_results.txt
    #(time python3 QuickSort.py) 2>> $TIME_FILE

#done

#for i in {1..30}
#do
#      powerjoular measures the power consumption while running your_python_script.py
#      Append the result to power_results.txt
#      python3 QuickSort.py &
#      PYTHON_PID=$!
#      powerjoular -p $PYTHON_PID >> $POWER_FILE_PYTHON &
#      POWERJOULAR_PID=$!
#      sleep 4
#      kill -INT $POWERJOULAR_PID
#      wait $PYTHON_PID

#  done


############################################
############################################
############################################

# Name of the files to store results
# TIME_FILE="time_results/time_results_c.txt"
#POWER_FILE="power_results_c.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# C implementation
#gcc QuickSort.c -o QuickSortC
#for i in {1..30}
#do
 #   (time ./QuickSortC) 2>> $TIME_FILE
#done

# for i in {1..30}
# do
#     ./executableC &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_C &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cO2.txt"
#POWER_FILE="power_results_c.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C implementation
gcc -o2 QuickSort.c -o QuickSortCo2
for i in {1..10}
do
    (time ./QuickSortCo2) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./QuickSortCo2 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_C &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cO3.txt"
#POWER_FILE="power_results_c.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C implementation
gcc -o3 QuickSort.c -o QuickSortCo3
for i in {1..10}
do
    (time ./QuickSortCo3) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./QuickSortCo2 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_C &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done


############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_cpp.txt"
#POWER_FILE="power_results_cpp.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# C++ implementation
#g++ QuickSort.cpp -o QuickSortCpp

# for i in {1..30}
# do
#     (time ./QuickSortCpp) 2>> $TIME_FILE
# done

# for i in {1..30}
# do
#     ./QuickSortCpp &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_CPP &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cppO2.txt"
#POWER_FILE="power_results_cpp.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C++ implementation
g++ -o2 QuickSort.cpp -o QuickSortCppO2

for i in {1..10}
do
    (time ./QuickSortCppO2) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./QuickSortCppO2 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_CPP &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cppO3.txt"
#POWER_FILE="power_results_cpp.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C++ implementation
g++ -o3 QuickSort.cpp -o QuickSortCppO3

for i in {1..10}
do
    (time ./QuickSortCppO3) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./QuickSortCppO3 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_CPP &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
# TIME_FILE="time_results/time_results_java.txt"
#POWER_FILE="power_results_java.txt"

# Clean any existing result files
# > $TIME_FILE
#> $POWER_FILE

# Java implementation
# javac QuickSort.java

# for i in {1..30}
# do

#     (time java QuickSort) 2>> $TIME_FILE
# done

# for i in {1..30}
# do
#     java QuickSor.java &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_JAVA &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_ruby.txt"
#POWER_FILE="power_results_ruby.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# Ruby implementation
#for i in {1..30}
#do

 #   (time ruby QuickSort.rb) 2>> $TIME_FILE
#done

# for i in {1..30}
# do
#     java Cal.java &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_JAVA &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done
