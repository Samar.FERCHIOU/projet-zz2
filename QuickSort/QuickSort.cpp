#include <iostream>
#include <vector>
constexpr int ARRAY_SIZE = 1020000;

void quicksort(std::vector<int>& arr, int low, int high) {
    if (low < high) {
        int pivot = arr[high];
        int i = low - 1;

        for (int j = low; j <= high - 1; ++j) {
            if (arr[j] < pivot) {
                ++i;
                std::swap(arr[i], arr[j]);
            }
        }

        std::swap(arr[i + 1], arr[high]);

        int partitionIndex = i + 1;

        quicksort(arr, low, partitionIndex - 1);
        quicksort(arr, partitionIndex + 1, high);
    }
}

int main() {
    // std::vector<int> arrayToSort = {64, 34, 25, 12, 22, 11, 90, 80, 45, 72,
    //                         65, 87, 92, 53, 29, 18, 14, 15, 33, 27,
    //                         10, 85, 59, 38, 49, 39, 56, 68, 42, 74,
    //                         70, 67, 88, 61, 76, 48, 99, 91, 19, 63,
    //                         20, 73, 21, 36, 97, 37, 54, 95, 84, 30,
    //                         78, 41, 40, 43, 52, 77, 94, 60, 98, 83,
    //                         17, 32, 23, 69, 57, 31, 28, 46, 81, 86,
    //                         96, 50, 82, 35, 89, 47, 55, 66, 75, 13,
    //                         62, 51, 16, 44, 24, 26, 58, 71, 79, 93};

    std::vector<int> arrayToSort(ARRAY_SIZE);
    for (int i = 0; i < ARRAY_SIZE; i++) {
        arrayToSort[i] = (i * 151 + 73) % 100000;
    }

    int arraySize = arrayToSort.size();

    quicksort(arrayToSort, 0, arraySize - 1);

    //std::cout << "Sorted array: ";
    //for (int element : arrayToSort) {
      //  std::cout << element << " ";
    //}

    return 0;
}
