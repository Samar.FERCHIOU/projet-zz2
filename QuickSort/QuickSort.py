def quicksort(arr):
    if len(arr) <= 1:
        return arr

    pivot = arr[-1]
    left = [x for x in arr if x < pivot]
    middle = [x for x in arr if x == pivot]
    right = [x for x in arr if x > pivot]

    return quicksort(left) + middle + quicksort(right)

ARRAY_SIZE = 1020000
# array_to_sort = [64, 34, 25, 12, 22, 11, 90, 80, 45, 72,
#                             65, 87, 92, 53, 29, 18, 14, 15, 33, 27,
#                             10, 85, 59, 38, 49, 39, 56, 68, 42, 74,
#                             70, 67, 88, 61, 76, 48, 99, 91, 19, 63,
#                             20, 73, 21, 36, 97, 37, 54, 95, 84, 30,
#                             78, 41, 40, 43, 52, 77, 94, 60, 98, 83,
#                             17, 32, 23, 69, 57, 31, 28, 46, 81, 86,
#                             96, 50, 82, 35, 89, 47, 55, 66, 75, 13,
#                             62, 51, 16, 44, 24, 26, 58, 71, 79, 93]

array_to_sort = [(i * 151 + 73) % 100000 for i in range(ARRAY_SIZE)]

sorted_array = quicksort(array_to_sort)

#print(sorted_array)
