class QuickSort
    def swap(arr, i, j)
      temp = arr[i]
      arr[i] = arr[j]
      arr[j] = temp
    end
  
    def partition(arr, low, high)
      pivot = arr[high]
      i = low - 1
  
      for j in low..high - 1
        if arr[j] <= pivot
          i += 1
          swap(arr, i, j)
        end
      end
  
      swap(arr, i + 1, high)
      return i + 1
    end
  
    def quicksort(arr, low, high)
      if low < high
        pi = partition(arr, low, high)
        quicksort(arr, low, pi - 1)
        quicksort(arr, pi + 1, high)
      end
    end
  end
  
  # Exemple d'utilisation :
  # array_to_sort = = [64, 34, 25, 12, 22, 11, 90, 80, 45, 72,
  # 65, 87, 92, 53, 29, 18, 14, 15, 33, 27,
  # 10, 85, 59, 38, 49, 39, 56, 68, 42, 74,
  # 70, 67, 88, 61, 76, 48, 99, 91, 19, 63,
  # 20, 73, 21, 36, 97, 37, 54, 95, 84, 30,
  # 78, 41, 40, 43, 52, 77, 94, 60, 98, 83,
  # 17, 32, 23, 69, 57, 31, 28, 46, 81, 86,
  # 96, 50, 82, 35, 89, 47, 55, 66, 75, 13,
  # 62, 51, 16, 44, 24, 26, 58, 71, 79, 93]

  ARRAY_SIZE = 1020000
  array_to_sort  = (0...ARRAY_SIZE).map { |i| (i * 151 + 73) % 100000 }
  
  qs = QuickSort.new
  qs.quicksort(array_to_sort, 0, ARRAY_SIZE - 1)
  
  #puts "Sorted array: #{array_to_sort.join(' ')}"
  