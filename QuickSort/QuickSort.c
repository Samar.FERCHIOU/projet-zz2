#include <stdio.h>

#define ARRAY_SIZE 1020000

void swap(int* a, int* b) {
    int t = *a;
    *a = *b;
    *b = t;
}

int partition(int arr[], int low, int high) {
    int pivot = arr[high];
    int i = (low - 1);

    for (int j = low; j <= high - 1; j++) {
        if (arr[j] <= pivot) {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }

    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

void quicksort(int arr[], int low, int high) {
    if (low < high) {
        int pi = partition(arr, low, high);

        quicksort(arr, low, pi - 1);
        quicksort(arr, pi + 1, high);
    }
}

// Example usage:
int main() {
    // int array_to_sort[100] = {64, 34, 25, 12, 22, 11, 90, 80, 45, 72,
    //                         65, 87, 92, 53, 29, 18, 14, 15, 33, 27,
    //                         10, 85, 59, 38, 49, 39, 56, 68, 42, 74,
    //                         70, 67, 88, 61, 76, 48, 99, 91, 19, 63,
    //                         20, 73, 21, 36, 97, 37, 54, 95, 84, 30,
    //                         78, 41, 40, 43, 52, 77, 94, 60, 98, 83,
    //                         17, 32, 23, 69, 57, 31, 28, 46, 81, 86,
    //                         96, 50, 82, 35, 89, 47, 55, 66, 75, 13,
    //                         62, 51, 16, 44, 24, 26, 58, 71, 79, 93};

    int array_to_sort[ARRAY_SIZE];
    
    for (int i = 0; i < ARRAY_SIZE; i++) {
        array_to_sort[i] = (i * 151 + 73) % 100000;
    }

   
    quicksort(array_to_sort, 0, ARRAY_SIZE - 1);

    // printf("Sorted array: ");
    //  for (int i = 0; i < ARRAY_SIZE; i++) {
    //     printf("%d ", array_to_sort[i]);
    // }


    return 0;
}
