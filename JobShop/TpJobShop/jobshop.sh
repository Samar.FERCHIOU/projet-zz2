# Name of the files to store results
TIME_FILE="time_results/time_results_cpp.txt"
#POWER_FILE="power_results_cpp.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C++ implementation
g++ JobShop.cpp  main.cppp -o js

for i in {1..3}
do
    (time ./js) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./QuickSortCpp &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_CPP &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cppO2.txt"
#POWER_FILE="power_results_cpp.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C++ implementation
g++ -O2 JobShop.cpp  main.cppp -o js2

for i in {1..3}
do
    (time ./js2) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./QuickSortCppO2 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_CPP &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cppO3.txt"
#POWER_FILE="power_results_cpp.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C++ implementation
g++ -O3 JobShop.cpp  main.cppp -o js3

for i in {1..3}
do
    (time ./js3) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./QuickSortCppO3 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_CPP &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done
