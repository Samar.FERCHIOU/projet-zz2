#include "Header.h"

// Lecture du contenu d'une instance et transformation en problème 
void lire_fichier_instance(t_probleme& P, string nom)
{
    std::ifstream fichier(nom.c_str());
    if (fichier.is_open())
    {
        fichier >> P.nb_pieces;
        fichier >> P.nb_machines;
        for (int i = 1; i <= P.nb_pieces; i++)
        {
            for (int j = 1; j <= P.nb_machines; j++)
            {

                fichier >> P.machine[i][j];
                fichier >> P.duree[i][j];
            }
        }
        fichier.close();
    }
}

// Lecture des valeurs optimales
void lire_fichier_optimale(int opt[nb_max_job], string nom)
{
    std::ifstream fichier(nom.c_str());
    int nombre_instances;

    if (fichier.is_open())
    {
        fichier >> nombre_instances;

        for (int i = 1; i <= nombre_instances; i++)
        {
            fichier >> opt[i];
        }
        fichier.close();
    }
}

// Génére un vecteur de Bierwwirth aléatoire 
void generer_vecteur(t_probleme P, t_solution& S)
{
    int taille_vecteur = P.nb_machines * P.nb_pieces;
    int indice;
    int nb_apparitions[nb_max_job] = { 0 };
    int i = 1;

    while (i <= taille_vecteur)
    {
        indice = (std::rand() % P.nb_pieces) + 1;
        if (nb_apparitions[indice] < P.nb_machines)
        {
            nb_apparitions[indice]++;
            S.vecteur[i] = indice;
            i++;
        }
    }
}

// Evalue une solution donnée
void evaluer_solution(t_probleme P, t_solution& S)
{
    t_couple nb_machine[nb_max_machine]; // Tableau pour suivre le nombre de jobs exécutés sur chaque machine
    int nb_job[nb_max_job] = { 0 }; // Tableau qui stocke le nombre de fois que chaque job a été exécuté
    int j;
    int date;
    int machine;
    int duree;
    int st_derniere_op;
    int num_job_final = 0;

    // initialition des données 
    initialiser_nmachine(nb_machine);
    initialiser_St(S.St);
    initialiser_Pere(S.Pere);
    S.cout = 0;

    // On parcourt le vecteur
    for (int i = 1; i <= P.nb_machines * P.nb_pieces; i++)
    {
        j = S.vecteur[i]; // numéro de la pièce 
        nb_job[j]++; // // incrémente le nombre de fois que ce job a été exécuté

        // Vérification s'il ne s'agit pas de la première occurrence de la pièce
        if (nb_job[j] > 1)
        {
            date = S.St[j][nb_job[j] - 1]; // temps de fin du job précédent
            duree = P.duree[j][nb_job[j] - 1]; // durée du job précédent

            if (date + duree > S.St[j][nb_job[j]])
            {
                // Mise à jour du temps de début dans la solution
                S.St[j][nb_job[j]] = duree + date;
                S.Pere[j][nb_job[j]].num_piece = j;
                S.Pere[j][nb_job[j]].rang = nb_job[j] - 1;
            }
        }
        machine = P.machine[j][nb_job[j]]; // la machine pour le job actuel

        // Vérification si la machine a été utilisée précédemment
        if (nb_machine[machine].num_piece != 0 && nb_machine[machine].rang != 0)
        {
            date = S.St[nb_machine[machine].num_piece][nb_machine[machine].rang]; // temps de fin du job précédent sur cette machine
            duree = P.duree[nb_machine[machine].num_piece][nb_machine[machine].rang]; // durée du job précédent sur cette machine
            if (date + duree > S.St[j][nb_job[j]])
            {
                // Mise à jour du temps de début dans la solution
                S.St[j][nb_job[j]] = date + duree;
                S.Pere[j][nb_job[j]].num_piece = nb_machine[machine].num_piece;
                S.Pere[j][nb_job[j]].rang = nb_machine[machine].rang;
            }
        }
        nb_machine[machine].num_piece = j;
        nb_machine[machine].rang = nb_job[j];
    }

    //recherche du job final

    // Calcul du temps de fin de la dernière opération pour chaque job
    for (int i = 1; i <= P.nb_pieces; i++)
    {
        st_derniere_op = S.St[i][P.nb_machines] + P.duree[i][P.nb_machines];
        if (st_derniere_op > S.cout)
        {
            S.cout = st_derniere_op;
            num_job_final = i;
        }
    }
    // Mise à jour de la solution avec le dernier job
    S.Pere[P.nb_pieces + 1][P.nb_machines + 1].num_piece = num_job_final;
    S.Pere[P.nb_pieces + 1][P.nb_machines + 1].rang = P.nb_machines;
    S.St[P.nb_pieces + 1][P.nb_machines + 1] = S.cout;
}

// Implémente la recherche locale
void recherche_locale(t_probleme P, t_solution& S, int nb_iter)
{
    int iter = 0;
    int position1;
    int position2;
    t_couple job;
    t_couple job_parent;
    t_solution nouvelle_solution;

    evaluer_solution(P, S);
    // on se met sur le sommet *
    job.num_piece = P.nb_pieces + 1;
    job.rang = P.nb_machines + 1;
    // on prend le père du sommet *
    job_parent.num_piece = S.Pere[job.num_piece][job.rang].num_piece;
    job_parent.rang = S.Pere[job.num_piece][job.rang].rang;

    while (iter < nb_iter && job_parent.num_piece != -1)
    {
        // Si on a 2 sommets de la même pièce
        if (job.num_piece == job_parent.num_piece || job.num_piece > P.nb_pieces)
        {
            // job prend la valeur de job_parent
            job.num_piece = job_parent.num_piece;
            job.rang = job_parent.rang;

            // on cherche le nouveau père de job 
            job_parent.num_piece = S.Pere[job.num_piece][job.rang].num_piece;
            job_parent.rang = S.Pere[job.num_piece][job.rang].rang;
        }
        else // si on a un arc disjonctif
        {
            // on permute la position de job et de job_parent dans le vecteur
            position1 = chercher_position_job_dans_vecteur(job, S.vecteur, P.nb_pieces, P.nb_machines);
            position2 = chercher_position_job_dans_vecteur(job_parent, S.vecteur, P.nb_pieces, P.nb_machines);
            permutation(S, nouvelle_solution, P.nb_pieces, P.nb_machines, position1, position2);

            if (check_vecteur(P.nb_pieces, P.nb_machines, nouvelle_solution.vecteur))
            {
                evaluer_solution(P, nouvelle_solution);
                // on garde la nouvelle solution si elle est meilleure
                if (nouvelle_solution.cout < S.cout)
                {
                    S = nouvelle_solution;
                    // on se met sur le sommet *
                    job.num_piece = P.nb_pieces + 1;
                    job.rang = P.nb_machines + 1;
                    // on prend le père du sommet *
                    job_parent.num_piece = S.Pere[job.num_piece][job.rang].num_piece;
                    job_parent.rang = S.Pere[job.num_piece][job.rang].rang;
                }
                else
                {
                    // on continue le parcours
                    job.num_piece = job_parent.num_piece;
                    job.rang = job_parent.rang;

                    // on cherche le nouveau père de job 
                    job_parent.num_piece = S.Pere[job.num_piece][job.rang].num_piece;
                    job_parent.rang = S.Pere[job.num_piece][job.rang].rang;
                }
            }
            else
            {
                std::cout << "Il y a une erreur";
                break;
            }


        }
        iter++;
    }
}

// Implémente la métaheuristique du GRASPxELS
void GRASP(t_probleme P, t_solution& meilleure_solution, int nb_els, int nb_grasp, int nb_voisins)
{
    t_solution meilleure_solution_els;
    t_solution voisin;
    t_solution meilleur_voisin;
    int pos1;
    int pos2;


    for (int k = 0; k < nb_grasp; k++)
    {
        // Génération d'une solution aléatoire
        generer_vecteur(P, meilleure_solution_els);
        evaluer_solution(P, meilleure_solution_els);

        meilleur_voisin = meilleure_solution_els;
        for (int i = 0; i < nb_els; i++)
        {
            // recherche du meilleur voisin
            for (int j = 0; j < nb_voisins; j++)
            {
                // recherche des deux indices à permuter
                do
                {
                    pos1 = (std::rand() % (P.nb_machines * P.nb_pieces)) + 1;
                    pos2 = (std::rand() % (P.nb_machines * P.nb_pieces)) + 1;
                } while (pos1 == pos2);

                // génération d'un voisin
                permutation(meilleure_solution_els, voisin, P.nb_pieces, P.nb_machines, pos1, pos2);

                if (check_vecteur(P.nb_pieces, P.nb_machines, voisin.vecteur))
                {
                    recherche_locale(P, voisin, 10);

                    //Mise à jour du meilleur voisin
                    if (voisin.cout < meilleur_voisin.cout)
                    {
                        meilleur_voisin = voisin;
                    }

                }
                else
                {
                    std::cout << "erreur";
                    break;
                }
            }
            // On retient le meilleur voisin
            meilleure_solution_els = meilleur_voisin;
        }
        // Mise à jour de la meilleure solution à l'issue de l'els
        if (k == 0 || meilleure_solution_els.cout < meilleure_solution.cout)
        {
            meilleure_solution = meilleure_solution_els;
        }
    }

}

// Cherche la position d'un job donné dans un vecteur 
int chercher_position_job_dans_vecteur(t_couple job, int vecteur[], int nb_pieces, int nb_machines)
{
    int position = 0;
    int taille = nb_pieces * nb_machines;
    int compteur = nb_machines + 1;

    for (int i = taille; i > 0; i--)
    {
        if (vecteur[i] == job.num_piece)
        {
            compteur--;
            if (compteur == job.rang)
            {
                position = i;
                break;
            }
        }
    }
    return position;
}

// Génère un nouveau vecteur en permutant deux positions dans le vecteur original
void permutation(t_solution S, t_solution& S2, int nb_pieces, int nb_machines, int p1, int p2)
{
    int x;

    for (int i = 1; i <= nb_pieces * nb_machines; i++)
    {
        S2.vecteur[i] = S.vecteur[i];
    }

    x = S2.vecteur[p1];
    S2.vecteur[p1] = S2.vecteur[p2];
    S2.vecteur[p2] = x;

}


// Implémente la fonction de hachage 
int fonction_hachage(t_solution S, int nb_pieces, int nb_machines)
{
    int somme = 0;
    for (int i = 1; i <= nb_pieces; i++)
        for (int j = 1; j <= nb_machines; j++)
            somme += S.St[i][j] * S.St[i][j];

    return somme % 1000000;

}

// Initialise le tableau St
void initialiser_St(int St[nb_max_job][nb_max_machine])
{
    for (int i = 0; i < nb_max_job; i++)
        for (int j = 0; j < nb_max_machine; j++)
            St[i][j] = 0;
}

// Initialise le tableau nmachine
void initialiser_nmachine(t_couple tab[])
{
    for (int i = 0; i < nb_max_machine; i++)
    {
        tab[i].num_piece = 0;
        tab[i].rang = 0;
    }
}

// Initialise le tableau Pere
void initialiser_Pere(t_couple tab[nb_max_job][nb_max_machine])
{
    for (int i = 0; i < nb_max_job; i++)
    {
        for (int j = 0; j < nb_max_machine; j++)
        {
            tab[i][j].num_piece = -1;
            tab[i][j].rang = -1;
        }
    }

}

// Vérifie si un vecteur est valide
bool check_vecteur(int nb_pieces, int nb_machines, int vecteur[])
{
    int nb_apparitions[nb_max_job] = { 0 };
    bool resultat = true;
    int j = 1;
    for (int i = 1; i <= nb_pieces * nb_machines; i++)
    {
        nb_apparitions[vecteur[i]]++;
    }
    while ((j <= nb_pieces) && resultat)
    {
        resultat = (nb_apparitions[j] == nb_machines);
        j++;
    }
    return resultat;

}


// Affiche un vecteur
void afficher_vecteur(int nb_pieces, int nb_machines, int vecteur[])
{
    for (int i = 1; i <= nb_pieces * nb_machines; i++)
    {
        std::cout << vecteur[i] << " ";
    }
    std::cout << endl;
}

// Affiche le chemin critique ainsi que les dates de début correspondantes
void afficher_chemin_critique(int nb_pieces, int nb_machines, t_solution S)
{
    t_couple operation;
    int job;
    int rang;

    operation.num_piece = nb_pieces + 1;
    operation.rang = nb_machines + 1;
    std::cout << "Chemin critique : ";
    while (operation.num_piece != -1)
    {
        cout << "O" << operation.num_piece << "," << operation.rang << " : " << S.St[operation.num_piece][operation.rang] << "  ";
        job = S.Pere[operation.num_piece][operation.rang].num_piece;
        rang = S.Pere[operation.num_piece][operation.rang].rang;
        operation.num_piece = job;
        operation.rang = rang;

    }
    cout << endl;
}