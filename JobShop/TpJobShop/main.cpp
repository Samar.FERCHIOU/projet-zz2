#include "Header.h"

int main()
{
    srand(time(nullptr));

    t_probleme* ptP = new t_probleme;
    t_probleme& P = *ptP;

    t_solution* ptS = new t_solution;
    t_solution& S = *ptS;


    int optimale[nb_max_job]; // Tableau pour stocker les valeurs optimales


    double difference; // Différence par rapport à l'optimal
    string num_instance;
    string nom_fichier;


    lire_fichier_optimale(optimale, "optimale.txt");

    for (int i = 1; i <= 20; i++)
    {
        // déterminer le nom du fichier de l'instance
        if (i < 10)
        {
            num_instance = "0" + to_string(i);
        }
        else
        {
            num_instance = to_string(i);
        }
        nom_fichier = "./instances/la" + num_instance + ".txt";

        lire_fichier_instance(P, nom_fichier);
        std::cout << "\033[31m" << "instance N°" << i << " :" << "\033[0m" << std::endl;

        auto start_time = std::chrono::high_resolution_clock::now();

        GRASP(P, S, 100, 120, 30);

        auto end_time = std::chrono::high_resolution_clock::now();

        // Calcul du temps d'exécution
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);

        // Calcul de la différence avec l'optimale
        difference = ((double)((S.cout - optimale[i])) / ((double)optimale[i])) * 100.0;

        // Affichage des résultats 
        std::cout << "Difference avec la solution optimale : " << difference << std::endl;
        std::cout << "Cout: " << "\033[32m" << S.cout << "\033[0m" << std::endl;
        std::cout << "Temps d'exécution: " << std::fixed << std::setprecision(3) << duration.count() << " millisecondes" << std::endl;
        afficher_chemin_critique(P.nb_pieces, P.nb_machines, S);
        std::cout << endl;
    }

    delete ptP;
    delete ptS;

}