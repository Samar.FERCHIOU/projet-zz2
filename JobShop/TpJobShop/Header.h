#pragma once
#ifndef _DEMO_
#define _DEMO_

#include <string>
#include <fstream>
#include <iostream>
#include <random>
#include <chrono>
#include <iomanip>

using namespace std;

const int nb_max_job = 30;
const int nb_max_machine = 30;

typedef struct t_probleme
{
	int nb_pieces;
	int nb_machines;
	int duree[nb_max_job][nb_max_machine];
	int machine[nb_max_job][nb_max_machine];
} t_probleme;

typedef struct t_couple
{
	int num_piece;
	int rang;

} t_couple;

typedef struct t_solution
{
	int cout;
	int vecteur[nb_max_job * nb_max_machine];
	int St[nb_max_job][nb_max_machine];
	t_couple Pere[nb_max_job][nb_max_machine];


} t_solution;

void lire_fichier_instance(t_probleme& P, string nom);

void lire_fichier_optimale(int opt[nb_max_job], string nom);

void generer_vecteur(t_probleme P, t_solution& S);

void evaluer_solution(t_probleme P, t_solution& S);

void recherche_locale(t_probleme P, t_solution& S, int nb_iter);

void GRASP(t_probleme P, t_solution& meilleure_solution, int nb_els, int nb_grasp, int nb_voisins);

void initialiser_St(int St[nb_max_job][nb_max_machine]);

void initialiser_nmachine(t_couple tab[]);

void initialiser_Pere(t_couple tab[nb_max_job][nb_max_machine]);

void afficher_vecteur(int nb_pieces, int nb_machines, int vecteur[]);

void afficher_chemin_critique(int nb_pieces, int nb_machines, t_solution S);

bool check_vecteur(int nb_pieces, int nb_machines, int vecteur[]);

void permutation(t_solution S, t_solution& S2, int nb_pieces, int nb_machines, int p1, int p2);

int chercher_position_job_dans_vecteur(t_couple job, int vecteur[], int nb_pieces, int nb_machines);

int fonction_hachage(t_solution S, int nb_pieces, int nb_machines);


#endif
