#include <stdio.h>

void factorise(int n) {
    printf("Factors of %d are: ", n);
    for (int i = 2; i <= n; ++i) {
        while (n % i == 0) {
            printf("%d ", i);
            n /= i;
        }
    }
    printf("\n");
}

int main() {
    int num = 9699690;
    factorise(num);
    return 0;
}
