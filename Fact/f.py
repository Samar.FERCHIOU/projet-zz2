def factorise(n):
    print(f"Factors of {n} are:", end=" ")
    for i in range(2, n + 1):
        while n % i == 0:
            print(i, end=" ")
            n //= i

num =  2 * 3 * 5 * 7 * 11 * 13 * 17 * 19
factorise(num)
