public class Prime {
    public static void factorise(int n) {
        System.out.print("Factors of " + n + " are: ");
        for (int i = 2; i <= n; ++i) {
            while (n % i == 0) {
                System.out.print(i + " ");
                n /= i;
            }
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int num = 9699690;
        factorise(num);
    }
}
