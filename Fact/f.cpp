#include <iostream>

void factorise(int n) {
    std::cout << "Factors of " << n << " are: ";
    for (int i = 2; i <= n; ++i) {
        while (n % i == 0) {
            std::cout << i << " ";
            n /= i;
        }
    }
    std::cout << std::endl;
}

int main() {
    int num = 9699690;
    factorise(num);
    return 0;
}
