def factorise(n)
    print "Factors of #{n} are: "
    i = 2
    while i <= n do
        while n % i == 0 do
            print "#{i} "
            n /= i
        end
        i += 1
    end
    puts
end

num = 9699690
factorise(num)
