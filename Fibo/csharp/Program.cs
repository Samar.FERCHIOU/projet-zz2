﻿using System;

class Program
{
    static long Fibb(int n)
    {
        if (n < 1) return 0;
        if (n < 2) return 1;
        return Fibb(n - 1) + Fibb(n - 2);
    }

    static void Main()
    {
        Console.WriteLine(Fibb(46));
    }
}
