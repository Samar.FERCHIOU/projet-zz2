#include <iostream>

long fibb(int n) {
    if (n < 1) return 0;
    if (n < 2) return 1;
    return fibb(n - 1) + fibb(n - 2);
}

int main() {
    std::cout << fibb(46) << std::endl;
    return 0;
}
