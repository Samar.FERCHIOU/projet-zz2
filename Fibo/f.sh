# #!/bin/bash


# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_python.txt"
# #POWER_FILE="power_results/power_results_python.txt"

# # Clean any existing result files
# #> $TIME_FILE
# #> $POWER_FILE

# # Python implementation
# # for i in {1..30}
# # do
# #      #Measure the time using built-in time and append to time_results.txt
# #     (time python3 f.py) 2>> $TIME_FILE

# # done

# #for i in {1..10}
# # do
# #     # powerjoular measures the power consumption while running your_python_script.py
# #     # Append the result to power_results.txt
#  #    python3 f.py &
#   #   PYTHON_PID=$!
#    #  powerjoular -p $PYTHON_PID >> $POWER_FILE &
#     # POWERJOULAR_PID=$!
#     # sleep 300
#     # kill -INT $POWERJOULAR_PID
#     # kill $PYTHON_PID

# # done


# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_c.txt"
# POWER_FILE="power_results/power_results_c.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation

# gcc f.c -o fC

# # for i in {1..30}
# # do
# #     (time ./fC) 2>> $TIME_FILE
# # done

# for i in {1..10}
# do
#     ./fC &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 10
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done


# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_cO2.txt"
# POWER_FILE="power_results/power_results_cO2.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation

# gcc -o2 f.c -o fCo2

# #for i in {1..10}
# #do
# #    (time ./fCo2) 2>> $TIME_FILE
# #done

#  for i in {1..10}
#  do
#      ./fCo2 &
#      PROGRAM_PID=$!
#      powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#      POWERJOULAR_PID=$!
#      sleep 10
#      kill -INT $POWERJOULAR_PID
#      kill $PROGRAM_PID
# done

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_cO3.txt"
# POWER_FILE="power_results/power_results_cO3.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation

# gcc -o3 f.c -o fCo3

# #for i in {1..10}
# #do
# #    (time ./fCo3) 2>> $TIME_FILE
# #done

#  for i in {1..10}
#  do
#      ./fCo3 &
#      PROGRAM_PID=$!
#      powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#      POWERJOULAR_PID=$!
#      sleep 10
#      kill -INT $POWERJOULAR_PID
#      kill $PROGRAM_PID
#  done

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_cpp.txt"
# POWER_FILE="power_results/power_results_cpp.txt"

# # Clean any existing result files
# #> $TIME_FILE
# #> $POWER_FILE

# # C++ implementation
# #g++ f.cpp -o fCpp

# # for i in {1..30}
# # do
# #     (time ./fCpp) 2>> $TIME_FILE
# # done

# #for i in {1..10}
# #do
# #    ./fCpp &
# #    PROGRAM_PID=$!
# #    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
# #    POWERJOULAR_PID=$!
# #    sleep 10
# #    kill -INT $POWERJOULAR_PID
# #    kill $PROGRAM_PID
# #done

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_cppO2.txt"
# POWER_FILE="power_results/power_results_cppO2.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation

# g++ -o2 f.cpp -o fCppO2

# #for i in {1..10}
# #do
# #   (time ./fCppO2) 2>> $TIME_FILE
# #done

#  for i in {1..10}
#  do
#      ./fCppO2 &
#      PROGRAM_PID=$!
#      powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#      POWERJOULAR_PID=$!
#      sleep 10
#      kill -INT $POWERJOULAR_PID
#      kill $PROGRAM_PID
#  done


# ############################################
# ############################################
# ############################################
# # Name of the files to store results
# #TIME_FILE="time_results/time_results_cppO3.txt"
# POWER_FILE="power_results/power_results_cppO3.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation

# g++ -o3 f.cpp -o fCppO3

# #for i in {1..10}
# #do
# #    (time ./fCppO3) 2>> $TIME_FILE
# #done

#  for i in {1..10}
#  do
#      ./fCppO3 &
#      PROGRAM_PID=$!
#      powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#      POWERJOULAR_PID=$!
#      sleep 10
#      kill -INT $POWERJOULAR_PID
#      kill $PROGRAM_PID
#  done

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_java.txt"
# #POWER_FILE="power_results/power_results_java.txt"

# # Clean any existing result files
# #> $TIME_FILE
# #> $POWER_FILE

# # Java implementation
# #javac Fibo.java

# # for i in {1..10}
# # do

# #     (time java Fibo) 2>> $TIME_FILE
# # done

# #for i in {1..10}
# #do
# #    java Fibo.java &
# #    PROGRAM_PID=$!
# #    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
# #    POWERJOULAR_PID=$!
# #    sleep 8
# #    kill -INT $POWERJOULAR_PID
# #    kill $PROGRAM_PID
# #done


# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# #TIME_FILE="time_results/time_results_ruby.txt"
# #POWER_FILE="power_results/power_results_ruby.txt"

# # Clean any existing result files
# #> $TIME_FILE
# #> $POWER_FILE

# # Ruby implementation
# # for i in {1..30}
# # do

# #     (time ruby f.rb) 2>> $TIME_FILE
# # done

# # for i in {1..10}
# # do
# #    ruby f.rb &
# #    PROGRAM_PID=$!
# #    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
# #    POWERJOULAR_PID=$!
# #    sleep 200
# #    kill -INT $POWERJOULAR_PID
# #    kill $PROGRAM_PID
# #done



ORIGINAL_DIR=$(pwd)
# Name of the files to store results
TIME_FILE="$ORIGINAL_DIR/time_results/time_results_csharp.txt"
POWER_FILE="$ORIGINAL_DIR/power_results/power_results_csharp.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

cd csharp

# Csharp implementation
/home/samar/.dotnet/dotnet build

#for i in {1..10}
#do
#    (time /home/samar/.dotnet/dotnet run) 2>> $TIME_FILE
#done

for i in {1..10}
do
     /home/samar/.dotnet/dotnet run &
     PROGRAM_PID=$!
     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
     POWERJOULAR_PID=$!
     sleep 19
     kill -INT $POWERJOULAR_PID
     kill -INT $PROGRAM_PID
done

cd "$ORIGINAL_DIR"

