public class Fibo {
    public static long fibb(int n) {
        if (n < 1) return 0;
        if (n < 2) return 1;
        return fibb(n - 1) + fibb(n - 2);
    }

    public static void main(String[] args) {
        System.out.println(fibb(46));
    }
}
