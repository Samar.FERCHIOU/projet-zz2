 def fibb(n)
    return 0 if n < 1
    return 1 if n < 2
    return fibb(n - 1) + fibb(n - 2)
  end
  
  puts fibb(46)
  