def merge(arr, l, m, r)
    n1 = m - l + 1
    n2 = r - m
  
    # Utilisation de variables locales au lieu de constantes
    l_array = arr[l..m]
    r_array = arr[m+1..r]
  
    i = j = 0
    k = l
  
    while i < n1 && j < n2
      if l_array[i] <= r_array[j]
        arr[k] = l_array[i]
        i += 1
      else
        arr[k] = r_array[j]
        j += 1
      end
      k += 1
    end
  
    while i < n1
      arr[k] = l_array[i]
      i += 1
      k += 1
    end
  
    while j < n2
      arr[k] = r_array[j]
      j += 1
      k += 1
    end
  end
  
  def merge_sort(arr, l, r)
    if l < r
      m = (l + r) / 2
  
      merge_sort(arr, l, m)
      merge_sort(arr, m + 1, r)
  
      merge(arr, l, m, r)
    end
  end
  
  # arr = [64, 34, 25, 12, 22, 11, 90, 80, 45, 72,
  #        65, 87, 92, 53, 29, 18, 14, 15, 33, 27,
  #        10, 85, 59, 38, 49, 39, 56, 68, 42, 74,
  #        70, 67, 88, 61, 76, 48, 99, 91, 19, 63,
  #        20, 73, 21, 36, 97, 37, 54, 95, 84, 30]
  
  ARRAY_SIZE = 1020000


  arr = Array.new(ARRAY_SIZE) { |i| (i * 151 + 73) % 100000 }

  #puts "Given array is: #{arr}"
  
  merge_sort(arr, 0, arr.length - 1)
  
  #puts "\nSorted array is: #{arr}"
  