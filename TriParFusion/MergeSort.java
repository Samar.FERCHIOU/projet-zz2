public class MergeSort {
    void merge(int arr[], int l, int m, int r) {
        int n1 = m - l + 1;
        int n2 = r - m;

        int L[] = new int[n1];
        int R[] = new int[n2];

        for (int i = 0; i < n1; ++i)
            L[i] = arr[l + i];
        for (int j = 0; j < n2; ++j)
            R[j] = arr[m + 1 + j];

        int i = 0, j = 0;

        int k = l;
        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }

        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    void sort(int arr[], int l, int r) {
        if (l < r) {
            int m = (l + r) / 2;

            sort(arr, l, m);
            sort(arr, m + 1, r);

            merge(arr, l, m, r);
        }
    }
            
    public static final int ARRAY_SIZE = 1020000;

    public static void main(String args[]) {
        // int arr[] = {64, 34, 25, 12, 22, 11, 90, 80, 45, 72,
        //              65, 87, 92, 53, 29, 18, 14, 15, 33, 27,
        //              10, 85, 59, 38, 49, 39, 56, 68, 42, 74,
        //              70, 67, 88, 61, 76, 48, 99, 91, 19, 63,
        //              20, 73, 21, 36, 97, 37, 54, 95, 84, 30};


        int[] arr = new int[ARRAY_SIZE];

        for (int i = 0; i < ARRAY_SIZE; i++) {
            arr[i] = (i * 151 + 73) % 100000;
        }
        
        // System.out.println("Given Array");
        // for (int i = 0; i < arr.length; i++)
        //     System.out.print(arr[i] + " ");
        // System.out.println();

        MergeSort ob = new MergeSort();
        ob.sort(arr, 0, arr.length - 1);

        // System.out.println("\nSorted array");
        // for (int i = 0; i < arr.length; ++i)
        //     System.out.print(arr[i] + " ");
        // System.out.println();
    }
}
