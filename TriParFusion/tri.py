def mergeSort(arr):
    if len(arr) > 1:
        mid = len(arr) // 2
        L = arr[:mid]
        R = arr[mid:]

        mergeSort(L)
        mergeSort(R)

        i = j = k = 0

        while i < len(L) and j < len(R):
            if L[i] < R[j]:
                arr[k] = L[i]
                i += 1
            else:
                arr[k] = R[j]
                j += 1
            k += 1

        while i < len(L):
            arr[k] = L[i]
            i += 1
            k += 1

        while j < len(R):
            arr[k] = R[j]
            j += 1
            k += 1

# arr = [64, 34, 25, 12, 22, 11, 90, 80, 45, 72,
#        65, 87, 92, 53, 29, 18, 14, 15, 33, 27,
#         10, 85, 59, 38, 49, 39, 56, 68, 42, 74,
#         70, 67, 88, 61, 76, 48, 99, 91, 19, 63,
#         20, 73, 21, 36, 97, 37, 54, 95, 84, 30]

ARRAY_SIZE = 1020000

arr = [(i * 151 + 73) % 100000 for i in range(ARRAY_SIZE)]

#print("Given array is", arr)

mergeSort(arr)

#print("Sorted array is", arr)
