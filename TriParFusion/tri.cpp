#include <iostream>
#include <vector>

using namespace std;
constexpr int ARRAY_SIZE = 1020000;

void merge(int* arr, int l, int m, int r) {
    int n1 = m - l + 1;
    int n2 = r - m;

    vector<int> L(n1), R(n2);

    for (int i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (int j = 0; j < n2; j++)
        R[j] = arr[m + 1 + j];

    int i = 0, j = 0, k = l;

    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            arr[k] = L[i];
            i++;
        } else {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    while (i < n1) {
        arr[k] = L[i];
        i++;
        k++;
    }

    while (j < n2) {
        arr[k] = R[j];
        j++;
        k++;
    }
}

//void mergeSort(vector<int>& arr, int l, int r) {
void mergeSort(int* arr, int l, int r) {
    if (l < r) {
        int m = l + (r - l) / 2;

        mergeSort(arr, l, m);
        mergeSort(arr, m + 1, r);

        merge(arr, l, m, r);
    }
}

int main() {
    // vector<int> arr = {64, 34, 25, 12, 22, 11, 90, 80, 45, 72,
    //                         65, 87, 92, 53, 29, 18, 14, 15, 33, 27,
    //                         10, 85, 59, 38, 49, 39, 56, 68, 42, 74,
    //                         70, 67, 88, 61, 76, 48, 99, 91, 19, 63,
    //                         20, 73, 21, 36, 97, 37, 54, 95, 84, 30};
    // int arr_size = arr.size();

    int arr[ARRAY_SIZE];
    
    for (int i = 0; i < ARRAY_SIZE; i++) {
        arr[i] = (i * 151 + 73) % 100000;
    }


    // cout << "Given array is \n";
    // for (int i = 0; i < ARRAY_SIZE; i++)
    //     cout << arr[i] << " ";
    // cout << "\n";

    mergeSort(arr, 0, ARRAY_SIZE - 1);

    // cout << "\nSorted array is \n";
    // for (int i = 0; i < ARRAY_SIZE; i++)
    //     cout << arr[i] << " ";
    // cout << "\n";
    return 0;
}
