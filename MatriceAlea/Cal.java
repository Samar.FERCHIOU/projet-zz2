public class Cal {
    static final int SIZE = 100;

    public static void main(String[] args) {
        float[][] A = new float[SIZE][SIZE];
        float[][] B = new float[SIZE][SIZE];
        float[][] C = new float[SIZE][SIZE];

        // Initialize matrices A and B with random values
        MTRandom rand = new MTRandom();
        for (int i = 0; i < SIZE; ++i) {
            for (int j = 0; j < SIZE; ++j) {
                A[i][j] = rand.nextFloat();
                B[i][j] = rand.nextFloat();
            }
        }

        
        for (int i = 0; i < SIZE; ++i) {
            for (int j = 0; j < SIZE; ++j) {
                C[i][j] = 0.0f;
                for (int k = 0; k < SIZE; ++k) {
                    C[i][j] += A[i][k] * B[k][j];
                }
            }
        }
        
    }
}
