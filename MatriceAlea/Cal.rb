require 'mersenne_twister'

SIZE = 100
SEED = 12345

# Initialiser le générateur Mersenne Twister
mt = MersenneTwister.new(SEED)

# Initialiser les matrices A et B avec des valeurs aléatoires entre 0.0 et 1.0
A = Array.new(SIZE) { Array.new(SIZE) { mt.rand } }
B = Array.new(SIZE) { Array.new(SIZE) { mt.rand } }
C = Array.new(SIZE) { Array.new(SIZE, 0.0) }

# Effectuer la multiplication de matrices
for i in 0...SIZE
  for j in 0...SIZE
    for k in 0...SIZE
      C[i][j] += A[i][k] * B[k][j]
    end
  end
end

puts "Matrix multiplication completed."
