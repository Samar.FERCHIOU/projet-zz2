#include <iostream>
#include <vector>
#include "MersenneTwister.hpp"
#include <iomanip> 
#include <fstream> 


using namespace std;


const int SIZE = 100;

int main() {

    uint32_t init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
 	mt = new MersenneTwister(init, length);

    std::vector<std::vector<float>> A(SIZE, std::vector<float>(SIZE));
    std::vector<std::vector<float>> B(SIZE, std::vector<float>(SIZE));
    std::vector<std::vector<float>> C(SIZE, std::vector<float>(SIZE));


     for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            A[i][j] = mt->genrand_real2();
            B[i][j] = mt->genrand_real2();
        }
    }


    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            C[i][j] = 0.0f;
            for (int k = 0; k < SIZE; ++k) {
                C[i][j] += A[i][k] * B[k][j];
            }
            std::cout <<C[i][j]<< " ";
        }
    }

    // Enregistrement des matrices dans des fichiers texte
    //ofstream file_A("matrix_Acpp.txt");
    //ofstream file_B("matrix_Bcpp.txt");
    // ofstream file_C("results/matrix_cpp.txt", ios::app);

    // // //if (!file_A || !file_B || !file_C) {
    // if (!file_C) {
    //     cerr << "Impossible d'ouvrir les fichiers pour enregistrement des matrices." << endl;
    //     return 1;
    // }
    // file_C << fixed << setprecision(6);

    // for (int i = 0; i < SIZE; ++i) {
    //     for (int j = 0; j < SIZE; ++j) {
    //         //file_A << A[i][j] << " ";
    //         //file_B << B[i][j] << " ";
    //         file_C << C[i][j] << " ";
    //     }
    //     //file_A << endl;
    //     //file_B << endl;
    //     file_C << endl;
    // }

    // file_C << endl << endl;

    delete mt;
    return 0;
}


