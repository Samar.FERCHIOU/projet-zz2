import numpy as np
from Mt import MersenneTwister
import time

SIZE = 100

init_key = [0x123, 0x234, 0x345, 0x456]
mt = MersenneTwister()
mt.init_by_array(init_key,4)
        
x = mt.genrand_int32() 


# Initialize matrices A and B using Mersenne Twister
A = np.zeros((SIZE, SIZE), dtype=np.float32)
B = np.zeros((SIZE, SIZE), dtype=np.float32)

for i in range(SIZE):
    for j in range(SIZE):
        A[i, j] = mt.genrand_real2()
        B[i, j] = mt.genrand_real2()

C = np.zeros((SIZE, SIZE), dtype=np.float32)


#C = np.dot(A, B)

for i in range(SIZE):
    for j in range(SIZE):
        for k in range(SIZE):
            C[i][j] += A[i][k] * B[k][j]

# # Enregistrer la matrice C dans un fichier texte
# with open("results/matrix_py.txt", "a") as file_A:
#     for i in range(SIZE):
#         for j in range(SIZE):
#             #file_A.write(str(C[i][j]) + " ")
#             file_A.write("{:.6f} ".format(C[i][j]))
#         file_A.write("\n")
#     file_A.write("\n\n")

#print(C)

