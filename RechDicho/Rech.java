public class Rech {
    static int binarySearch(int arr[], int l, int r, int x) {
        while (l <= r) {
            int m = l + (r - l) / 2;

            if (arr[m] == x)
                return m;

            if (arr[m] < x)
                l = m + 1;
            else
                r = m - 1;
        }

        return -1;
    }

    public static void main(String[] args) {
 
        int arr[] = new int[10000000];
        for (int i = 0; i < 10000000; ++i) {
            arr[i] = i + 1;
        }
        
        int n = arr.length;
        int x = -1; // Élément recherché
        int result = binarySearch(arr, 0, n - 1, x);
        if (result == -1)
            System.out.println("L'élément n'est pas présent dans le tableau");
        else
            System.out.println("L'élément est présent à l'index " + result);
    }
}
