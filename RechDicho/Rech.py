def binarySearch(arr, l, r, x):
    while l <= r:
        m = l + (r - l) // 2

        if arr[m] == x:
            return m

        elif arr[m] < x:
            l = m + 1
        else:
            r = m - 1

    return -1

# Tableau de taille 100 avec des valeurs de 1 à 100
arr = [i + 1 for i in range(10000000)]

x = -1  # Élément recherché
result = binarySearch(arr, 0, len(arr) - 1, x)
if result != -1:
    print("L'élément est présent à l'index", result)
else:
    print("L'élément n'est pas présent dans le tableau")
