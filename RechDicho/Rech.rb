def binary_search(arr, x)
    l = 0
    r = arr.length - 1
  
    while l <= r
      m = l + (r - l) / 2
      
      if arr[m] == x
        return m
      elsif arr[m] < x
        l = m + 1
      else
        r = m - 1
      end
    end
  
    return -1
  end
  
  # Tableau de taille 100 avec des valeurs de 1 à 100
  arr = (1..10000000).to_a
  
  x = -1 # Élément recherché
  result = binary_search(arr, x)
  
  if result == -1
    puts "L'élément n'est pas présent dans le tableau"
  else
    puts "L'élément est présent à l'index #{result}"
  end
  