#include <iostream>
using namespace std;

int binarySearch(int arr[], int l, int r, int x) {
    while (l <= r) {
        int m = l + (r - l) / 2;
        
        if (arr[m] == x)
            return m;
        
        if (arr[m] < x)
            l = m + 1;
        else
            r = m - 1;
    }
    
    return -1;
}

int main() {
   
    int n = 10000000;
    int* arr = new int[n];

    for (int i = 0; i < 100; ++i) {
        arr[i] = i + 1;
    }
    
    int x = -1; // Élément recherché
    int result = binarySearch(arr, 0, n - 1, x);
    (result == -1) ? cout << "L'élément n'est pas présent dans le tableau" << endl
                   : cout << "L'élément est présent à l'index " << result << endl;
    
    delete[] arr;
    return 0;
}