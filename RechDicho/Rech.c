#include <stdio.h>
#include <stdlib.h>

int binarySearch(int arr[], int l, int r, int x) {
    while (l <= r) {
        int m = l + (r - l) / 2;
        
        if (arr[m] == x)
            return m;
        
        if (arr[m] < x)
            l = m + 1;
        else
            r = m - 1;
    }
    
    return -1;
}

int main() {
    
    int n = 10000000;
    int *arr = (int *)malloc(n * sizeof(int));

    for (int i = 0; i < n; ++i) {
        arr[i] = i + 1;
    }
    
    int x = -1; // Élément recherché
    int result = binarySearch(arr, 0, n - 1, x);
    if (result != -1) {
        printf("L'élément est présent à l'index %d\n", result);
    } else {
        printf("L'élément n'est pas présent dans le tableau\n");
    }

    free(arr);
    return 0;
}

