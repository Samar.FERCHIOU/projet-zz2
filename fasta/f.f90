program fasta

  implicit none
  integer :: num, m
  character(len=8) :: argv
  logical, dimension(:), allocatable :: flags
  integer, parameter :: IM = 139968
  integer, parameter :: IA = 3877
  integer, parameter :: IC = 29573
  character(len=*), parameter :: alu = &
'GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG' // &
'GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA' // &
'CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT' // &
'ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA' // &
'GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG' // &
'AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC' // &
'AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA'

  type :: pair
     character(len=1) :: c
     real*8 :: p
  end type pair
  type(pair), dimension(15) :: iub
  type(pair), dimension(4)  :: homosapiens
  homosapiens = [ pair('a', 0.3029549426680d0), &
                   pair('c', 0.1979883004921d0), &
                   pair('g', 0.1975473066391d0), &
                   pair('t', 0.3015094502008d0) ]
  call makeCumulative(homosapiens)
  
  iub = [ pair('a', 0.27d0), &
           pair('c', 0.12d0), &
           pair('g', 0.12d0), &
           pair('t', 0.27d0), &
           pair('B', 0.02d0), &
           pair('D', 0.02d0), &
           pair('H', 0.02d0), &
           pair('K', 0.02d0), &
           pair('M', 0.02d0), &
           pair('N', 0.02d0), &
           pair('R', 0.02d0), &
           pair('S', 0.02d0), &
           pair('V', 0.02d0), &
           pair('W', 0.02d0), &
           pair('Y', 0.02d0) ]
  call makeCumulative(iub)

  call getarg(1,argv)
  read(argv,*) num

  ! Ouvrir le fichier pour l'écriture
  open(unit=10, file='results/fastaFF.txt', status='replace')

  ! Appeler les sous-programmes pour générer les séquences et les écrire dans le fichier
  call makeRepeatFasta('ONE','Homo sapiens alu',alu,num*2, 10)
  call makeRandomFasta('TWO','IUB ambiguity codes',iub,num*3, 10)
  call makeRandomFasta('THREE','Homo sapiens frequency',homosapiens,num*5, 10)

  ! Fermer le fichier
  close(unit=10)

contains

  ! Définition des sous-programmes makeCumulative, selectRandom et getRandom

end program fasta
