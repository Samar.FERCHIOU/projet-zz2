
// #include <cstdio>
// #include <cstdlib>
// #include <cstring>
// #include <algorithm>
// #include <vector>
// #include <numeric>
// #include <iostream>
// #include <fstream>

// static int const IM = 139968, IA = 3877, IC = 29573;
// static int last = 42;

// static inline
// float
// genRandom(float max)
// {
//    return(max * (last = (last * IA + IC) % IM) / IM);
// }

// struct IUB
// {
//    char c;
//    float p;
// };

// static inline
// void
// makeCumulative(std::vector<IUB>& i)
// {
//    std::partial_sum (i.begin(), i.end(), i.begin(), 
//    [](IUB l,IUB r)->IUB{r.p+=l.p;return r;});
// }

// static const char alu[] =
// "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG"
// "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA"
// "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT"
// "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA"
// "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG"
// "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC"
// "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA";

// static const unsigned length = 60;

// template <class F>
// static inline
// void
// make(char const * id, char const * desc, unsigned n, F f)
// {
//    printf(">%s %s\n", id, desc);
//    char line[length+1]={0};
//    unsigned i = 0;
//    while(n-- > 0)
//    {
//       line[i++]=f();
//       if(i >= length)
//       {
//          puts(line);
//          i = 0;
//       }
//    }
//    line[i] = 0;
//    if(strlen(line))puts(line);
// }

// struct Repeat{
//    Repeat(const char* alu):i(0),size(strlen(alu)),alu(alu){}
//    char operator()()
//    { 
//       if(i >= size)i=0;
//       return alu[i++];
//    }
//    unsigned i,size;
//    const char* alu;
// };

// struct Random{
//    Random(std::vector<IUB>& i):i(i){}
//    char operator()()
//    {
//       float p = genRandom(1.0);
//       return i[std::count_if(i.begin(),i.end(),
//             [p](IUB i){ return p>=i.p;})].c;
//    }
//    std::vector<IUB>& i;
// };

// static std::vector<IUB> iub =
// {
//    { 'a', 0.27 },
//    { 'c', 0.12 },
//    { 'g', 0.12 },
//    { 't', 0.27 },

//    { 'B', 0.02 },
//    { 'D', 0.02 },
//    { 'H', 0.02 },
//    { 'K', 0.02 },
//    { 'M', 0.02 },
//    { 'N', 0.02 },
//    { 'R', 0.02 },
//    { 'S', 0.02 },
//    { 'V', 0.02 },
//    { 'W', 0.02 },
//    { 'Y', 0.02 }
// };

// static std::vector<IUB> homosapiens =
// {
//    { 'a', 0.3029549426680 },
//    { 'c', 0.1979883004921 },
//    { 'g', 0.1975473066391 },
//    { 't', 0.3015094502008 }
// };

// int main(int argc, char *argv[]) {
//     unsigned const n = argc > 1 ? atoi(argv[1]) : 1;

//     // Ouvrir un fichier pour écrire les résultats
//     std::ofstream outputFile("results/fastaCpp.txt");

//     // Rediriger la sortie standard (stdout) vers le fichier
//     std::streambuf *coutbuf = std::cout.rdbuf(); // Sauvegarder le tampon de cout
//     std::cout.rdbuf(outputFile.rdbuf()); // Rediriger cout vers le fichier

//     // Générer les résultats et les écrire dans le fichier
//     makeCumulative(iub);
//     makeCumulative(homosapiens);

//     make("ONE", "Homo sapiens alu", n*2,Repeat(alu));
//     make("TWO", "IUB ambiguity codes", n*3, Random(iub));
//     make("THREE", "Homo sapiens frequency", n*5, Random(homosapiens));

//     // Fermer le fichier et restaurer la sortie standard
//     outputFile.close();
//     std::cout.rdbuf(coutbuf); // Restaurer le tampon de cout

//     return 0;
// }

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

constexpr int IM = 139968;
constexpr int IA = 3877;
constexpr int IC = 29573;
constexpr int LINE_LENGTH = 60;

double gen_random(double max) {
    static long last = 42;
    return max * (last = (last * IA + IC) % IM) / IM;
}

struct AminoAcids {
    char c;
    double p;
};

void make_cumulative(std::vector<AminoAcids>& genelist) {
    double cp = 0.0;
    for (auto& aa : genelist) {
        cp += aa.p;
        aa.p = cp;
    }
}

char select_random(const std::vector<AminoAcids>& genelist) {
    double r = gen_random(1);
    auto it = std::lower_bound(genelist.begin(), genelist.end(), r,
        [](const AminoAcids& aa, double r) { return aa.p < r; });
    if (it != genelist.end())
        return it->c;
    else
        return genelist.back().c;
}

void make_random_fasta(const std::string& id, const std::string& desc, const std::vector<AminoAcids>& genelist, int n, std::ofstream& file) {
    file << ">" << id << " " << desc << "\n";
    for (int todo = n; todo > 0; todo -= LINE_LENGTH) {
        int m = std::min(todo, LINE_LENGTH);
        std::string pick;
        for (int i = 0; i < m; ++i)
            pick += select_random(genelist);
        file << pick << "\n";
    }
}

void make_repeat_fasta(const std::string& id, const std::string& desc, const std::string& s, int n, std::ofstream& file) {
    file << ">" << id << " " << desc << "\n";
    int k = 0;
    for (int todo = n; todo > 0; todo -= LINE_LENGTH) {
        int m = std::min(todo, LINE_LENGTH);
        if (m >= s.size() - k) {
            file << s.substr(k);
            m -= s.size() - k;
            k = 0;
        }
        file << s.substr(k, m) << "\n";
        k = (k + m) % s.size();
    }
}

int main(int argc, char* argv[]) {
    int n = 1000;
    if (argc > 1)
        n = std::stoi(argv[1]);

    std::ofstream file("results/fastaCpp.txt", std::ios::app);
    if (!file.is_open()) {
        std::cerr << "Error opening file." << std::endl;
        return 1;
    }

    std::vector<AminoAcids> iub = {
        { 'a', 0.27 }, { 'c', 0.12 }, { 'g', 0.12 }, { 't', 0.27 },
        { 'B', 0.02 }, { 'D', 0.02 }, { 'H', 0.02 }, { 'K', 0.02 },
        { 'M', 0.02 }, { 'N', 0.02 }, { 'R', 0.02 }, { 'S', 0.02 },
        { 'V', 0.02 }, { 'W', 0.02 }, { 'Y', 0.02 }
    };

    std::vector<AminoAcids> homosapiens = {
        { 'a', 0.3029549426680 }, { 'c', 0.1979883004921 },
        { 'g', 0.1975473066391 }, { 't', 0.3015094502008 }
    };

    make_cumulative(iub);
    make_cumulative(homosapiens);

    std::string alu =
        "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG"
        "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA"
        "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT"
        "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA"
        "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG"
        "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC"
        "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA";

    make_repeat_fasta("ONE", "Homo sapiens alu", alu, n * 2, file);
    make_random_fasta("TWO", "IUB ambiguity codes", iub, n * 3, file);
    make_random_fasta("THREE", "Homo sapiens frequency", homosapiens, n * 5, file);

    file << "\n\n";
    file.close();

    return 0;
}
