#!/bin/bash

rm -f results/fastaC.txt
rm -f results/fastaJava.txt
rm -f results/fastaCpp.txt
rm -f results/fastaPy.txt
rm -f results/fastaRb.txt

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_python.txt"
#POWER_FILE="power_results/power_results_python.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# Python implementation
#for i in {1..5}
#do
    # Measure the time using built-in time and append to time_results.txt
#    (time python3 f.py 10000000) 2>> $TIME_FILE

#done

#for i in {1..5}
# do
#     python3 f.py 10000000 &
#     PYTHON_PID=$!
#     powerjoular -p $PYTHON_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PYTHON_PID

# done


############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_c.txt"
#POWER_FILE="power_results/power_results_c.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# C implementation
#gcc f.c -o fC
#for i in {1..10}
#do
#    (time ./fC 10000000) 2>> $TIME_FILE
#done

# for i in {1..10}
# do
#     ./fC 10000000 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cO2.txt"
#POWER_FILE="power_results/power_results_cO2.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# C implementation
#gcc -o2 f.c -o fC
#for i in {1..10}
#do
 #   (time ./fC 10000000) 2>> $TIME_FILE
 #   rm -f results/fastaC.txt
#done

# for i in {1..10}
# do
#     ./fC 10000000 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cO3.txt"
#POWER_FILE="power_results/power_results_cO3.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# C implementation
#gcc -o3 f.c -o fC
#for i in {1..10}
#do
#    (time ./fC 10000000) 2>> $TIME_FILE
#    rm -f results/fastaC.txt
#done

# for i in {1..10}
# do
#     ./fC 10000000 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cpp.txt"
#POWER_FILE="power_results/power_results_cpp.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# C++ implementation
#g++ f.cpp -o fCpp

#for i in {1..10}
#do
#    (time ./fCpp 10000000) 2>> $TIME_FILE
#done

# for i in {1..10}
# do
#     ./fCpp 10000000 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cppO2.txt"
#POWER_FILE="power_results/power_results_cppO2.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# C++ implementation o2
#g++ -o2 f.cpp -o fCpp

#for i in {1..10}
#do
  #  (time ./fCpp 10000000) 2>> $TIME_FILE
 #   rm -f results/fastaCpp.txt
#done

# for i in {1..10}
# do
#     ./fCpp 10000000 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_cppO3.txt"
#POWER_FILE="power_results/power_results_cppO3.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# C++ implementation
#g++ -o3 f.cpp -o fCpp

#for i in {1..10}
#do
 #   (time ./fCpp 10000000) 2>> $TIME_FILE
 #   rm -f results/fastaCpp.txt
#done

# for i in {1..10}
# do
#     ./fCpp 10000000 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done


############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_java.txt"
#POWER_FILE="power_results/power_results_java.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# Java implementation
#javac fasta.java

#for i in {1..10}
#do

 #   (time java fasta 10000000) 2>> $TIME_FILE
 #   rm -f results/fastaJava.txt
#done

#rm -f results/fastaJava.txt
# for i in {1..30}
# do
#     java fasta 10000000 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done


############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results/time_results_ruby.txt"
#POWER_FILE="power_results/power_results_ruby.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# Ruby implementation
for i in {1..5}
do

    (time ruby f.rb 10000000) 2>> $TIME_FILE
    rm -f results/fastaRb.txt
done

# for i in {1..10}
# do
#     ruby f.rb 10000000 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done
