import random
import math
from Mt import MersenneTwister

class Etats:
    Susceptible = 0
    Exposed = 1
    Infected = 2
    Recovered = 3

class Agent:
    def __init__(self, etat, mt):
        self.etat = etat
        self.x = mt.genrand_int32() % 300
        self.y = mt.genrand_int32() % 300
        self.calcul_param(mt)
        self.dEtatCourant = 0

    def get_etat(self):
        return self.etat 

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def get_dI(self):
        return self.dI

    def loi_exponentielle(self, lambda_val,mt):
        u = mt.genrand_real2()
        return -math.log(1 - u) * lambda_val

    def calcul_param(self, mt):
        self.dE = int(self.loi_exponentielle(3.0, mt))
        self.dI = int(self.loi_exponentielle(7.0, mt))
        self.dR = int(self.loi_exponentielle(365.0, mt))

    def se_deplacer(self, grille, mt):
        if self.etat == Etats.Infected:
            grille.decrementer_case(self.x, self.y)
        self.x = mt.genrand_int32() % 300
        self.y = mt.genrand_int32() % 300

    def changer_etat(self, grille, mt):
        if self.etat == Etats.Infected:
            self.recuperation()
        elif self.etat == Etats.Exposed:
            self.exposition()
        elif self.etat == Etats.Susceptible:
            self.infection(grille, mt)
        else:
            self.susceptibilite()
        if self.etat == Etats.Infected:
            grille.incrementer_case(self.x, self.y)

    def infection(self, grille, mt):
        p = 1 - math.exp(-0.5 * grille.nb_voisins(self))
        u = mt.genrand_real2() 

        if u < p:
            self.etat = Etats.Exposed
            self.dEtatCourant = 1
        else:
            self.dEtatCourant += 1

    def exposition(self):
        if self.dEtatCourant > self.dE:
            self.etat = Etats.Infected
            self.dEtatCourant = 1
        else:
            self.dEtatCourant += 1

    def recuperation(self):
        if self.dEtatCourant > self.dI:
            self.etat = Etats.Recovered
            self.dEtatCourant = 1
        else:
            self.dEtatCourant += 1

    def susceptibilite(self):
        if self.dEtatCourant > self.dR:
            self.etat = Etats.Susceptible
            self.dEtatCourant = 1
        else:
            self.dEtatCourant += 1


class Grille:
    NOMBRE_DE_LIGNES = 300
    NOMBRE_DE_COLONNES = 300

    def __init__(self):
        self.grille = [[0] * self.NOMBRE_DE_COLONNES for _ in range(self.NOMBRE_DE_LIGNES)]

    def get_case(self, x, y):
        return self.grille[x][y]

    def incrementer_case(self, x, y):
        self.grille[x][y] += 1

    def decrementer_case(self, x, y):
        self.grille[x][y] -= 1

    def nb_voisins(self, agent):
        x = agent.get_x()
        y = agent.get_y()
        Ni = self.get_case(x, y)

        dx = [-1, -1, -1, 0, 0, 1, 1, 1]
        dy = [-1, 0, 1, -1, 1, -1, 0, 1]

        for i in range(8):
            voisinX = (x + dx[i] + self.NOMBRE_DE_LIGNES) % self.NOMBRE_DE_LIGNES
            voisinY = (y + dy[i] + self.NOMBRE_DE_COLONNES) % self.NOMBRE_DE_COLONNES
            Ni += self.get_case(voisinX, voisinY)

        return Ni


class Population:
    def __init__(self, grille, mt):
        self.agents = []
        for i in range(20):
            agent = Agent(Etats.Infected, mt)
            self.agents.append(agent)
            grille.incrementer_case(agent.get_x(), agent.get_y())
            #print("i ", agent.get_x(), agent.get_y(), agent.get_dI())

        for _ in range(19980):
            self.agents.append(Agent(Etats.Susceptible, mt))
    
    @staticmethod
    def melanger(agents, mt):
        for i in range(20000 - 1, 0, -1):
            j = mt.genrand_int32() % (i + 1)
            temp = agents[i]
            agents[i] = agents[j]
            agents[j] = temp


    def faire_evoluer(self, grille, mt):
        for agent in self.agents:
            agent.se_deplacer(grille, mt)
            agent.changer_etat(grille, mt)

    def resultat(self):
        resultat = [0, 0, 0, 0]
        for agent in self.agents:
            etat = agent.get_etat()
            if etat == Etats.Infected:
                resultat[0] += 1
            elif etat == Etats.Exposed:
                resultat[1] += 1
            elif etat == Etats.Susceptible:
                resultat[2] += 1
            else:
                resultat[3] += 1
        return resultat 


class Simulation:
    def __init__(self):
        self.resultats = []

    @staticmethod
    def resultat_to_csv(file_writer, resultat):
         file_writer.write(','.join(map(lambda val: '{:.6f}'.format(val / 20000.0), resultat)) + '\n')
    
    @staticmethod
    def main():

        init_key = [0x123, 0x234, 0x345, 0x456]
        mt = MersenneTwister()
        mt.init_by_array(init_key,4)
        
        x = mt.genrand_int32() 

        for j in range(100):
        
            csv_file_name = f"resultats/exp{j}.csv"

            with open(csv_file_name, 'w') as writer:
                writer.write("I,E,S,R\n")
                simulation = Simulation()
                grille = Grille()
                population = Population(grille, mt)
                
                resultat = population.resultat()
                #print(resultat)

                for _ in range(730):
                    resultat = [0, 0, 0, 0]
                    population.melanger(population.agents, mt)
                    population.faire_evoluer(grille, mt)
                    resultat = population.resultat()
                    #print(resultat)
                    simulation.resultat_to_csv(writer, resultat)

    

if __name__ == "__main__":
    Simulation.main()
