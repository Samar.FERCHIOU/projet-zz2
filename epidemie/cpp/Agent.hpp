#ifndef AGENT_HPP
#define AGENT_HPP

#include <iostream>
#include <cmath>
#include "Grille.hpp"

class Grille;

// Déclaration de l'énumération pour les états
enum class Etats { Susceptible, Exposed, Infected, Recovered };

// Déclaration de la classe Agent
class Agent {
private:
    Etats etat;
    int dE;
    int dI;
    int dR;
    int dEtatCourant;
    int x;
    int y;

public:
    // Constructeur
    
    Agent(Etats etat);

    // Méthodes d'accès
    Etats getEtat();
    int getX();
    int getY();

    // Loi exponentielle
    double loiExponentielle(double lambda);

    // Calcul des paramètres
    void calculParam();

    // Déplacement de l'agent
    void seDeplacer(Grille&);

    // Changer l'état de l'agent
    void changerEtat(Grille&);

    // Processus d'infection
    void infection(Grille&);

    // Processus d'exposition
    void exposition();

    // Processus de récupération
    void recuperation();

    // Processus de susceptibilité
    void susceptibilite();
};

#endif