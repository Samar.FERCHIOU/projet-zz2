#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include "../MersenneTwister.hpp"
#include "../Grille.hpp"
#include "../Population.hpp"


// Fonction pour écrire les résultats dans un fichier CSV
void resultatToCSV(std::ofstream& writer, const std::vector<int>& resultat) {
    for (int i = 0; i < 4; i++) {
        writer <<  std::fixed << std::setprecision(6) << static_cast<double>(resultat[i]) / 20000.0;
        if (i < 3) {
            writer << ",";
        }
    }
    writer << "\n";
}



int main() {

    uint32_t init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
 	mt = new MersenneTwister(init, length);

    // Création de 100 fichiers CSV avec des noms différents
    std::string csvFileName;
    for (int j = 0; j < 100; j++) {
        csvFileName = "resultats/exp" + std::to_string(j) + ".csv";

        // Ouverture du fichier CSV en écriture
        std::ofstream writer(csvFileName);
        if (!writer.is_open()) {
            std::cerr << "Erreur lors de l'ouverture du fichier " << csvFileName << std::endl;
            return 1;
        }

        // Écriture de l'en-tête dans le fichier CSV
        writer << "I,E,S,R\n";

        // Simulation
        Grille grille;
        std::vector<int> resultat;
        for (int k = 0; k < 4; k++) {
                resultat.push_back(0);
            }
        Population population(grille); 
        population.resultat(resultat); 

        for (int i = 0; i < 730; i++) {
            for (int k = 0; k < 4; k++) {
                resultat[k] = 0;
            }
            population.melanger(); 
            population.faireEvoluer(grille); 
            population.resultat(resultat); 
            resultatToCSV(writer, resultat);
        }

        // Fermeture du fichier CSV
        writer.close();
       // std::cout << mt->compteur;
    }

    return 0;
}

