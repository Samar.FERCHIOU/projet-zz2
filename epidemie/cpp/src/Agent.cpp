#include "../Agent.hpp"
#include "../MersenneTwister.hpp"

// Constructeur
Agent::Agent(Etats etat) : etat(etat) {
    dEtatCourant = 0;
    x = mt->genrand_uint32() % 300;
    y = mt->genrand_uint32() % 300;
    MersenneTwister::compteur += 2;
    calculParam();
 
}

// Méthodes d'accès
Etats Agent::getEtat() {
    return etat;
}

int Agent::getX() {
    return x;
}

int Agent::getY() {
    return y;
}

// Loi exponentielle
double Agent::loiExponentielle(double lambda) {
    double u = mt->genrand_real2();
    MersenneTwister::compteur++;
    return -log(1 - u) * lambda;
}

// Calcul des paramètres
void Agent::calculParam() {
    dE = (loiExponentielle(3.0));
    dI = (loiExponentielle(7.0));
    dR = (loiExponentielle(365.0));
}

// Déplacement de l'agent
void Agent::seDeplacer(Grille& grille) {
    if (etat == Etats::Infected)
        grille.decrementerCase(x, y);
    x = mt->genrand_uint32() % 300;
    y = mt->genrand_uint32() % 300; 
    MersenneTwister::compteur+= 2;
}

// Changer l'état de l'agent
void Agent::changerEtat(Grille& grille) {
    switch (etat) {
        case Etats::Infected:
            recuperation();
            break;
        case Etats::Exposed:
            exposition();
            break;
        case Etats::Susceptible:
            infection(grille);
            break;
        default:
            susceptibilite();
            break;
    }
    if (etat == Etats::Infected)
        grille.incrementerCase(x, y);
}

// Processus d'infection
void Agent::infection(Grille& grille) {
    double p;
    double u = mt ->genrand_real1();
    MersenneTwister::compteur++;
    int Ni = grille.nbVoisins(this);
    p = 1 - exp(-0.5 * Ni);
    if (u < p) {
        etat = Etats::Exposed;
        dEtatCourant = 1;
    } else {
        dEtatCourant++;
    }
}

// Processus d'exposition
void Agent::exposition() {
    if (dEtatCourant > dE) {
        etat = Etats::Infected;
        dEtatCourant = 1;
    } else {
        dEtatCourant++;
    }
}

// Processus de récupération
void Agent::recuperation() {
    if (dEtatCourant > dI) {
        etat = Etats::Recovered;
        dEtatCourant = 1;
    } else {
        dEtatCourant++;
    }
}

// Processus de susceptibilité
void Agent::susceptibilite() {
    if (dEtatCourant > dR) {
        etat = Etats::Susceptible;
        dEtatCourant = 1;
    } else {
        dEtatCourant++;
    }
}

