#include "../Grille.hpp"

const int Grille::NOMBRE_DE_LIGNES = 300;
const int Grille::NOMBRE_DE_COLONNES = 300;

Grille::Grille() : grille(NOMBRE_DE_LIGNES, std::vector<int>(NOMBRE_DE_COLONNES, 0)) {}

int Grille::getCase(int x, int y) {
    return grille[x][y];
}

void Grille::incrementerCase(int x, int y) {
    grille[x][y]++;
}

void Grille::decrementerCase(int x, int y) {
    grille[x][y]--;
}

int Grille::nbVoisins(Agent* agent) {
    int x = agent->getX();
    int y = agent->getY();
    int Ni = getCase(x, y);

    int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1};
    int dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};

    for (int i = 0; i < 8; i++) {
        int voisinX = (x + dx[i] + NOMBRE_DE_LIGNES) % NOMBRE_DE_LIGNES;
        int voisinY = (y + dy[i] + NOMBRE_DE_COLONNES) % NOMBRE_DE_COLONNES;

        Ni += getCase(voisinX, voisinY);
    }
    return Ni;
}



// #include <vector>
// #include "Agent.hpp"

// class Grille {
// private:
//     std::vector<std::vector<int>> grille;
//     static const int NOMBRE_DE_LIGNES = 300;
//     static const int NOMBRE_DE_COLONNES = 300;

// public:
//     Grille() : grille(NOMBRE_DE_LIGNES, std::vector<int>(NOMBRE_DE_COLONNES, 0)) {}

//     int getCase(int x, int y) {
//         return grille[x][y];
//     }

//     void incrementerCase(int x, int y) {
//         grille[x][y]++;
//     }

//     void decrementerCase(int x, int y) {
//         grille[x][y]--;
//     }

//     int nbVoisins(Agent agent) {
//         int x = agent.getX();
//         int y = agent.getY();
//         int Ni = this->getCase(x, y);

//         int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1};
//         int dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};

//         for (int i = 0; i < 8; i++) {
//             int voisinX = (x + dx[i] + NOMBRE_DE_LIGNES) % NOMBRE_DE_LIGNES;
//             int voisinY = (y + dy[i] + NOMBRE_DE_COLONNES) % NOMBRE_DE_COLONNES;

//             Ni += this->getCase(voisinX, voisinY);
//         }
//         return Ni;
//     }
// };
