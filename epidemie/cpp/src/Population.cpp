#include "../Population.hpp"
#include "../MersenneTwister.hpp"


Population::Population(Grille& grille) {
    for (int i = 0; i < 20; i++) {
        agents.push_back(new Agent(Etats::Infected));
    //    std::cout << agents[i]->getX() << " " << agents[i]->getY() << std::endl;
        grille.incrementerCase(agents[i]->getX(), agents[i]->getY());
    }

    for (int i = 20; i < 20000; i++) {
        agents.push_back(new Agent(Etats::Susceptible));
       // std::cout << agents[i]->getX() << " " <<  agents[i]->getY() << std::endl;
    }
}
void Population::melanger() {
    for (int i = 20000 - 1; i > 0; i--) {
        int j = mt->genrand_uint32() % (i + 1);
        MersenneTwister::compteur++;
        std::swap(agents[i], agents[j]);
    }
}

void Population::faireEvoluer(Grille& grille) {
    for (Agent* agent : agents) {
        agent->seDeplacer(grille);
        agent->changerEtat(grille);
    }
}

void Population::resultat(std::vector<int>& resultat) {
    for (Agent* agent : agents) {
            switch (agent->getEtat()) {
                case Etats::Infected:
                    resultat[0]++;
                    break;
                case Etats::Exposed:
                    resultat[1]++;
                    break;
                case Etats::Susceptible:
                    resultat[2]++;
                    break;
                default:
                    resultat[3]++;
                    break;
            }
        }
}
