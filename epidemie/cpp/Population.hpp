#ifndef POPULATION_HPP
#define POPULATION_HP

#include "Agent.hpp"
#include "Grille.hpp"
#include <vector>

class Population {
private:
    std::vector<Agent*> agents;

public:
    Population(Grille&);

    void melanger();

    void faireEvoluer(Grille& );

    void resultat(std::vector<int>&);
};

#endif
