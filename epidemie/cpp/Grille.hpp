#ifndef GRILLE_HPP
#define GRILLE_HPP

#include <vector>
#include "Agent.hpp"

class Agent;

class Grille {
private:
    std::vector<std::vector<int>> grille;
    static const int NOMBRE_DE_LIGNES;
    static const int NOMBRE_DE_COLONNES;

public:
    Grille();

    int getCase(int x, int y);

    void incrementerCase(int x, int y);

    void decrementerCase(int x, int y);

    int nbVoisins(Agent* agent);
};

#endif