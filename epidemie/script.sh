#!/bin/bash

ORIGINAL_DIR=$(pwd)

# # Name of the files to store results
# TIME_FILE="$ORIGINAL_DIR/time_results/time_results_cpp.txt"
# POWER_FILE="$ORIGINAL_DIR/power_results/power_results_cpp.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# cd cpp

# # C++ implementation
# make

# #for i in {1..10}
# #do
# #    (time ./simulation) 2>> $TIME_FILE
# #done

# for i in {1..5}
# do
#     ./simulation &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 120
#     kill -INT $POWERJOULAR_PID
#     kill -INT $PROGRAM_PID
# done

# cd "$ORIGINAL_DIR"

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="$ORIGINAL_DIR/time_results/time_results_csharp.txt"
POWER_FILE="$ORIGINAL_DIR/power_results/power_results_csharp.txt"

# Clean any existing result files
#> $TIME_FILE
> $POWER_FILE

cd csharp

# Csharp implementation
/home/samar/.dotnet/dotnet build

#for i in {1..10}
#do
#    (time /home/samar/.dotnet/dotnet run) 2>> $TIME_FILE
#done

for i in {1..3}
do
    /home/samar/.dotnet/dotnet run &
    PROGRAM_PID=$!
    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
    POWERJOULAR_PID=$!
    sleep 450
    kill -INT $POWERJOULAR_PID
    kill -INT $PROGRAM_PID
done

cd "$ORIGINAL_DIR"

############################################
############################################
############################################

# # Name of the files to store results
# TIME_FILE="$ORIGINAL_DIR/time_results/time_results_java.txt"
# POWER_FILE="$ORIGINAL_DIR/power_results/power_results_java.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # Java implementation
# cd java


# #for i in {1..10}
# #do
# #    (time make run) 2>> $TIME_FILE
# #done

# for i in {1..5}
# do
#     make run &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 120
#     kill -INT $POWERJOULAR_PID
#     kill -INT $PROGRAM_PID
# done

# cd "$ORIGINAL_DIR"

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# TIME_FILE="$ORIGINAL_DIR/time_results/time_results_C.txt"
# POWER_FILE="$ORIGINAL_DIR/power_results/power_results_C.txt"

# # Clean any existing result files
# #> $TIME_FILE
# > $POWER_FILE

# # C implementation
# cd C

# gcc ep.c Mt.c -o p -lm
# #for i in {1..10}
# #do
# #    (time ./p) 2>> $TIME_FILE
# #done

# for i in {1..5}
# do
#     ./p &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 100
#     kill -INT $POWERJOULAR_PID
#     kill -INT $PROGRAM_PID
# done

# cd "$ORIGINAL_DIR"

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# TIME_FILE="$ORIGINAL_DIR/time_results/time_results_CO2.txt"
# POWER_FILE="$ORIGINAL_DIR/power_results/power_results_CO2.txt"

# # Clean any existing result files
# > $TIME_FILE
# > $POWER_FILE

# # C implementation
# cd C

# gcc -o2 ep.c Mt.c -o p2 -lm
# for i in {1..10}
# do
#     (time ./p2) 2>> $TIME_FILE
# done

# for i in {1..10}
# do
#     ./p2 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 60
#     kill -INT $POWERJOULAR_PID
#     kill -INT $PROGRAM_PID
# done

# cd "$ORIGINAL_DIR"

# ############################################
# ############################################
# ############################################

# # Name of the files to store results
# TIME_FILE="$ORIGINAL_DIR/time_results/time_results_CO3.txt"
# POWER_FILE="$ORIGINAL_DIR/power_results/power_results_CO3.txt"

# # Clean any existing result files
# > $TIME_FILE
# > $POWER_FILE

# # C implementation
# cd C

# gcc -o3 ep.c Mt.c -o p3 -lm
# for i in {1..10}
# do
#     (time ./p3) 2>> $TIME_FILE
# done

# for i in {1..10}
# do
#     ./p3 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 60
#     kill -INT $POWERJOULAR_PID
#     kill -INT $PROGRAM_PID
# done

# cd "$ORIGINAL_DIR"
############################################
############################################
############################################

# # Name of the files to store results
# TIME_FILE="$ORIGINAL_DIR/time_results/time_results_python.txt"
# #POWER_FILE="power_results_python.txt"

# # Clean any existing result files
# > $TIME_FILE
# #> $POWER_FILE

# # Pÿthon implementation
# cd python

# for i in {1..10}
# do
#     (time python ep.py) 2>> $TIME_FILE
# done

# # for i in {1..30}
# # do
# #     python ep.py &
# #     PROGRAM_PID=$!
# #     powerjoular -p $PROGRAM_PID >> $POWER_FILE_JAVA &
# #     POWERJOULAR_PID=$!
# #     sleep 4
# #     kill -INT $POWERJOULAR_PID
# #     wait $PROGRAM_PID
# # done

#cd "$ORIGINAL_DIR"

