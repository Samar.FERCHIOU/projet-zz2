﻿using System;
using System.Collections.Generic;


namespace MersenneTwister
{
    public class Population
    {
        private List<Agent> agents;

        public Population(Grille grille)
        {
            agents = new List<Agent>();

            for (int i = 0; i < 20; i++)
            {
                agents.Add(new Agent(Etats.Infected));
                grille.IncrementerCase(agents[i].x, agents[i].y);
            }

            for (int i = 20; i < 20000; i++)
            {
                agents.Add(new Agent(Etats.Susceptible));
            }
        }

        public void Melanger()
        {
            for (var i = 20000 - 1; i > 0; i--)
            {
                int j = (int) (Program.mt.genrand_uint32() % (ulong)(i + 1));
                Agent temp = agents[i];
                agents[i] = agents[j];
                agents[j] = temp;
            }
        }

        public void FaireEvoluer(Grille grille)
        {
            foreach (Agent agent in agents)
            {
                agent.SeDeplacer(grille);
                agent.ChangerEtat(grille);
            }
        }

        public void Resultat(List<int> resultat)
        {
            foreach (Agent agent in agents)
            {
                switch (agent.etat)
                {
                    // Handle other cases as needed
                    case Etats.Infected:
                        resultat[0]++;
                        break;
                    case Etats.Exposed:
                        resultat[1]++;
                        break;
                    case Etats.Susceptible:
                        resultat[2]++;
                        break;
                    case Etats.Recovered:
                        resultat[3]++;
                        break;
                }
            }
        }
    }
}
