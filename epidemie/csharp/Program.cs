using System;
using MersenneTwister;
using System.Collections.Generic;
using System.IO;
using PseudoRandom;
using System.Globalization;

class Program {

    public static PseudoRandom.MersenneTwister mt;

    static void Main()
    {
        mt = new PseudoRandom.MersenneTwister();

        // Create 100 CSV files with different names
        for (int j = 0; j < 100; j++)
        {
            string directoryPath = "resultats"; 
            string csvFileName = $"{directoryPath}/exp{j}.csv";

            // Create the directory if it doesn't exist
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            // Open the CSV file for writing
            using (StreamWriter writer = new StreamWriter(csvFileName))
            {
                if (writer == null)
                {
                    Console.WriteLine($"Erreur lors de l'ouverture du fichier {csvFileName}");
                    return;
                }

                // Write the header to the CSV file
                writer.WriteLine("I,E,S,R");

                // Simulation
                Grille grille = new Grille();
                List<int> resultat = new List<int> { 0, 0, 0, 0 };
                Population population = new Population(grille);
                population.Resultat(resultat);

                for (int i = 0; i < 730; i++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        resultat[k] = 0;
                    }
                    population.Melanger();
                    population.FaireEvoluer(grille);
                    population.Resultat(resultat);
                    ResultatToCSV(writer, resultat);
                }
            }
        }
    }

    // Function to write results to a CSV file
    static void ResultatToCSV(StreamWriter writer, List<int> resultat)
    {
        for (int i = 0; i < 4; i++)
        {
            writer.Write(((double)resultat[i] / 20000.0).ToString("0.000000", CultureInfo.InvariantCulture));
            if (i < 3)
            {
                writer.Write(",");
            }
        }
        writer.WriteLine();
    }
}
