﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MersenneTwister
{
    public enum Etats
    {
        Infected,
        Exposed,
        Susceptible,
        Recovered
    }
    public class Agent
    {
            public Etats etat { get; private set; }
            public int x { get; private set; }
            public int y { get; private set; }
            private int dEtatCourant;
            private int dE;
            private int dI;
            private int dR;


            public Agent(Etats etat)
            {
                this.etat = etat;
                dEtatCourant = 0;
                x = (int)(Program.mt.genrand_uint32() % 300);
                y = (int)(Program.mt.genrand_uint32() % 300);
               
                CalculParam();
            }

        public double LoiExponentielle(double lambda)
        {
            double u = Program.mt.genrand_real2();
           
            return -Math.Log(1 - u) * lambda;
        }

        private void CalculParam()
        {
            dE = (int) LoiExponentielle(3.0);
            dI = (int)LoiExponentielle(7.0);
            dR = (int)LoiExponentielle(365.0);
        }

        public void SeDeplacer(Grille grille)
        {
            if (etat == Etats.Infected)
                grille.DecrementerCase(x, y);
            x = (int)(Program.mt.genrand_uint32() % 300);
            y = (int)(Program.mt.genrand_uint32() % 300);
           
        }

        public void ChangerEtat(Grille grille)
        {
            switch (etat)
            {
                case Etats.Infected:
                    Recuperation();
                    break;
                case Etats.Exposed:
                    Exposition();
                    break;
                case Etats.Susceptible:
                    Infection(grille);
                    break;
                default:
                    Susceptibilite();
                    break;
            }
            if (etat == Etats.Infected)
                grille.IncrementerCase(x, y);
        }

        private void Infection(Grille grille)
        {
            double p;
            double u = Program.mt.genrand_real1();
            
            int Ni = grille.NbVoisins(this);
            p = 1 - Math.Exp(-0.5 * Ni);
            if (u < p)
            {
                etat = Etats.Exposed;
                dEtatCourant = 1;
            }
            else
            {
                dEtatCourant++;
            }
        }

        private void Exposition()
        {
            if (dEtatCourant > dE)
            {
                etat = Etats.Infected;
                dEtatCourant = 1;
            }
            else
            {
                dEtatCourant++;
            }
        }

        private void Recuperation()
        {
            if (dEtatCourant > dI)
            {
                etat = Etats.Recovered;
                dEtatCourant = 1;
            }
            else
            {
                dEtatCourant++;
            }
        }

        private void Susceptibilite()
        {
            if (dEtatCourant > dR)
            {
                etat = Etats.Susceptible;
                dEtatCourant = 1;
            }
            else
            {
                dEtatCourant++;
            }
        }
    
    }
}
