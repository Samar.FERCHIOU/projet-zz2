﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MersenneTwister
{
    public class Grille
    {
        public const int NombreDeLignes = 300;
        public const int NombreDeColonnes = 300;
        private List<List<int>> grille;

        public Grille()
        {
            grille = new List<List<int>>(NombreDeLignes);
            for (int i = 0; i < NombreDeLignes; i++)
            {
                grille.Add(new List<int>(NombreDeColonnes));
                for (int j = 0; j < NombreDeColonnes; j++)
                {
                    grille[i].Add(0);
                }
            }
        }

        public int GetCase(int x, int y)
        {
            return grille[x][y];
        }

        public void IncrementerCase(int x, int y)
        {
            grille[x][y]++;
        }

        public void DecrementerCase(int x, int y)
        {
            grille[x][y]--;
        }

        public int NbVoisins(Agent agent)
        {
            int x = agent.x;
            int y = agent.y;
            int Ni = GetCase(x, y);

            int[] dx = { -1, -1, -1, 0, 0, 1, 1, 1 };
            int[] dy = { -1, 0, 1, -1, 1, -1, 0, 1 };

            for (int i = 0; i < 8; i++)
            {
                int voisinX = (x + dx[i] + NombreDeLignes) % NombreDeLignes;
                int voisinY = (y + dy[i] + NombreDeColonnes) % NombreDeColonnes;

                Ni += GetCase(voisinX, voisinY);
            }
            return Ni;
        }
    }
}
