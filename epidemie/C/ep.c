#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "Mt.h"

#define NOMBRE_D_AGENT 20000
#define TAILLE_GRILLE 300
#define NB_JOURS 730
#define PROBA_INFECTION 0.5
#define LAMBDA_E 3.0
#define LAMBDA_I 7.0
#define LAMBDA_R 365.0

typedef enum {
    Susceptible,
    Exposed,
    Infected,
    Recovered
} Etats;

typedef struct {
    Etats etat;
    int dE;
    int dI;
    int dR;
    int dEtatCourant;
    int x;
    int y;
} Agent;

int compteur =0 ; 

double loiExponentielle(double lambda) {
    double u = genrand_real2(); 
     compteur++;
    return -log(1 - u) * lambda;
}

int randomNumber(int max) {
    compteur++;
    return (genrand_int32() % max); 
}

void initialiserGrille(int grille[TAILLE_GRILLE][TAILLE_GRILLE]) {
    for (int i = 0; i < TAILLE_GRILLE; i++) {
        for (int j = 0; j < TAILLE_GRILLE; j++) {
            grille[i][j] = 0;
        }
    }
}

void initialiserParametres(Agent *agent) {
    agent->dE = (int) loiExponentielle(LAMBDA_E);
    agent->dI = (int) loiExponentielle(LAMBDA_I);
    agent->dR = (int) loiExponentielle(LAMBDA_R);
}


void initialiserPopulation(int grille[TAILLE_GRILLE][TAILLE_GRILLE], Agent agents[NOMBRE_D_AGENT]) {
    // Initialisation des agents infectés
    for (int i = 0; i < 20; i++) {
        agents[i].etat = Infected;
        agents[i].x = randomNumber(TAILLE_GRILLE);
        agents[i].y = randomNumber(TAILLE_GRILLE);
        
        grille[agents[i].x][agents[i].y]++;
        agents[i].dEtatCourant =0;
                

        initialiserParametres(&agents[i]);
        //printf("i %d %d %d %d\n", agents[i].x, agents[i].y, agents[i].dI, agents[i].dEtatCourant);
    }

    // Initialisation des agents susceptibles
    for (int i = 20; i < NOMBRE_D_AGENT; i++) {
        agents[i].etat = Susceptible;
        agents[i].x = randomNumber(TAILLE_GRILLE);
        agents[i].y = randomNumber(TAILLE_GRILLE);
        agents[i].dEtatCourant =0;
              //  printf("s %d %d \n", agents[i].x, agents[i].y);

        initialiserParametres(&agents[i]);
    }
}


void incrementerCase(int grille[TAILLE_GRILLE][TAILLE_GRILLE], int x, int y) {
    grille[x][y]++;
}

void decrementerCase(int grille[TAILLE_GRILLE][TAILLE_GRILLE], int x, int y) {
    grille[x][y]--;
}

int nbVoisins(int grille[TAILLE_GRILLE][TAILLE_GRILLE], int x, int y) {
    int Ni = grille[x][y];

    int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1};
    int dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};

    for (int i = 0; i < 8; i++) {
        int voisinX = (x + dx[i] + TAILLE_GRILLE) % TAILLE_GRILLE;
        int voisinY = (y + dy[i] + TAILLE_GRILLE) % TAILLE_GRILLE;

        Ni += grille[voisinX][voisinY];
    }
    return Ni;
}

void seDeplacer(Agent *agent, int grille[TAILLE_GRILLE][TAILLE_GRILLE]) {
    if (agent->etat == Infected) {
        decrementerCase(grille, agent->x, agent->y);
    }

    agent->x = randomNumber(TAILLE_GRILLE) ; 
    agent->y = randomNumber(TAILLE_GRILLE) ;

}

void infection(Agent *agent, int grille[TAILLE_GRILLE][TAILLE_GRILLE]) {
    double p;
    double u = genrand_real1(); 
     compteur++;   
    //printf("u %f ",u);
    int Ni = nbVoisins(grille, agent->x,agent->y);
    p = 1 - exp(-0.5 * Ni);
    if (u < p) {
        agent->etat = Exposed;
        agent->dEtatCourant = 1;
    } else
        agent->dEtatCourant++;
}

void exposition(Agent *agent) {
    if (agent->dEtatCourant > agent->dE) {
        agent->etat = Infected;
        agent->dEtatCourant = 1;
    } else
        agent->dEtatCourant++;
}

void recuperation(Agent *agent) {
    if (agent->dEtatCourant > agent->dI) {
        agent->etat = Recovered;
        agent->dEtatCourant = 1;
    } else
        agent->dEtatCourant++;
}

void susceptibilite(Agent *agent) {
    if (agent->dEtatCourant > agent->dR) {
        agent->etat = Susceptible;
        agent->dEtatCourant = 1;
    } else
        agent->dEtatCourant++;
}


void changerEtat(Agent *agent,int grille[TAILLE_GRILLE][TAILLE_GRILLE]) {
    switch (agent->etat) {
        case Infected:
            recuperation(agent);
            break;
        case Exposed:
            exposition(agent);
            break;
        case Susceptible:
            infection(agent, grille);
            break;
        default:
            susceptibilite(agent);
            break;
    }

    if (agent->etat == Infected)
        incrementerCase(grille, agent->x, agent->y);
}


void melanger(Agent agents[NOMBRE_D_AGENT]) {
    for (int i = NOMBRE_D_AGENT - 1; i > 0; i--) {
         int j = (genrand_int32() % (i + 1));
          compteur++;
        //  if (i==  NOMBRE_D_AGENT - 1 )
        //  printf("%d ",j);
         
        Agent temp = agents[i];
        agents[i] = agents[j];
        agents[j] = temp;
    }
}

void faireEvoluer(Agent agents[NOMBRE_D_AGENT], int grille[TAILLE_GRILLE][TAILLE_GRILLE]) {
    for (int i = 0; i < NOMBRE_D_AGENT; i++) {
        seDeplacer(&agents[i], grille);
        changerEtat(&agents[i], grille);
    }
}

void resultat(Agent agents[NOMBRE_D_AGENT], int resultats[4]) {

    for (int i = 0; i < NOMBRE_D_AGENT; i++) {
        switch (agents[i].etat) {
            case Infected:
                resultats[0]++; 
                break;
            case Exposed:
                resultats[1]++;
                break;
            case Susceptible:
                resultats[2]++;
                break;
            case Recovered:
                resultats[3]++;
                break;
        }
    }
    //printf("I:%d E:%d S:%d R:%d \n", resultats[0],resultats[1],resultats[2],resultats[3]);
}

void resultatToCSV(FILE *file, int resultat[]) {
    for (int i = 0; i < 4; i++) {
        fprintf(file, "%.6lf", (double)resultat[i] / 20000.0); // Write the value divided by 20000.0
        //printf("%lf ", (double)resultat[i] / 20000.0);
        if (i < 3) {
            fprintf(file, ",");
        }
    }
   // printf("\n");
    fprintf(file, "\n");
}


int main() {


    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

     for(int j=0; j<100;j++)
      {
       
        char csvFileName[50]; // Assuming maximum length of resulting string
       // Construct the filename
       snprintf(csvFileName, sizeof(csvFileName), "resultats/exp%d.csv", j);
        

        int grille[TAILLE_GRILLE][TAILLE_GRILLE];
        Agent agents[NOMBRE_D_AGENT];
        int resultats[4] = {0};

        initialiserGrille(grille);

        // Initialisation de la population d'agents
        initialiserPopulation(grille, agents);


        FILE * file = fopen(csvFileName, "w");
        if (file == NULL) {
            printf("Error opening file.\n");
            return 1;
        }

        resultat(agents, resultats);
  

        fprintf(file, "I,E,S,R\n");
    
        // Simulation loop
        for (int jour = 0; jour < 730; jour++) {
            
            for (int k = 0; k < 4; k++) {
                resultats[k] = 0;
            }
            
                melanger(agents);
                faireEvoluer(agents, grille);
                resultat(agents, resultats);
              
                resultatToCSV(file, resultats);  
        }  

       // printf("%d ", compteur);  
      }

        // for (int k = 0; k < 2000; k++)
        //    {int x= genrand_int32() % 300;
        //    printf("%d ",x); }
      
    return 0;
}
