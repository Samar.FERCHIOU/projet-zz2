#!/bin/bash

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results_COM/time_results_python.txt"
#POWER_FILE="power_results_python.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# Python implementation
for i in {1..30}
do
    #Measure the time using built-in time and append to time_results.txt
    ( time python3 f.py 5) 2>> $TIME_FILE

done

#for i in {1..30}
# do
#     # powerjoular measures the power consumption while running your_python_script.py
#     # Append the result to power_results.txt
#     python3 your_python_script.py &
#     PYTHON_PID=$!
#     powerjoular -p $PYTHON_PID >> $POWER_FILE_PYTHON &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PYTHON_PID

# done


############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results_COM/time_results_c.txt"
#POWER_FILE="power_results_c.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C implementation

for i in {1..30}
do
    { time (gcc f.c -o fC && ./fC 5) ;} 2>> $TIME_FILE

done

# for i in {1..30}
# do
#     ./executableC &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_C &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done


############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results_COM/time_results_cpp.txt"
#POWER_FILE="power_results_cpp.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# C++ implementation


for i in {1..30}
do
    { time  (g++ f.cpp -o fCpp &&./fCpp 5);} 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     ./executableCpp &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_CPP &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done

############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results_COM/time_results_java.txt"
#POWER_FILE="power_results_java.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# Java implementation
#javac fannkuchredux.java

for i in {1..30}
do
    { time (javac fannkuchredux.java && java fannkuchredux 5); } 2>> $TIME_FILE
    #(time java fannkuchredux 5) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     java Cal.java &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_JAVA &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done


############################################
############################################
############################################

# Name of the files to store results
TIME_FILE="time_results_COM/time_results_ruby.txt"
#POWER_FILE="power_results_ruby.txt"

# Clean any existing result files
> $TIME_FILE
#> $POWER_FILE

# Ruby implementation
for i in {1..30}
do

    ( time ruby f.rb 5) 2>> $TIME_FILE
done

# for i in {1..30}
# do
#     java Cal.java &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE_JAVA &
#     POWERJOULAR_PID=$!
#     sleep 4
#     kill -INT $POWERJOULAR_PID
#     wait $PROGRAM_PID
# done
