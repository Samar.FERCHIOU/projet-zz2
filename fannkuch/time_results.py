import os
import re
import numpy as np
import matplotlib.pyplot as plt

def extract_time(time_str):
    match = re.match(r"(\w+)\t(\d+m)?(\d+),(\d+)s", time_str)
    if match:
        time_type = match.group(1)
        minutes = int(match.group(2)[:-1]) 
        seconds = float(match.group(3) + '.' + match.group(4))
        return minutes * 60 + seconds
    else:
        return 0
    
# Function to read and process data from a file
def process_file(filename, output_file ):
    real_times = []
    user_times = []
    sys_times = []

    with open(filename, 'r') as file:
        lines = file.readlines()
        for line in lines:
            if line.startswith('real'):
                real_times.append(extract_time(line))
            elif line.startswith('user'):
                user_times.append(extract_time(line))
            elif line.startswith('sys'):
                sys_times.append(extract_time(line))
    
    with open(output_file, 'a') as out_file:
        out_file.write(f"{filename}:\nreal: {np.mean(real_times)}, user: {np.mean(user_times)}, sys: {np.mean(sys_times)}\n\n")

    #print(filename, " : real : ",np.mean(real_times), " user : ", np.mean(user_times), " sys : ", np.mean(sys_times))
    return np.mean(real_times), np.mean(user_times), np.mean(sys_times)

# Function to plot the results
def plot_results(real_means, user_means, sys_means, filenames, output_fig):
    plt.figure(figsize=(10, 6))
    plt.plot(filenames, real_means, marker='o', label='Real Time')
    plt.plot(filenames, user_means, marker='o', label='User Time')
    plt.plot(filenames, sys_means, marker='o', label='Sys Time')
    plt.title('Fannkuch')
    plt.xlabel('File')
    plt.ylabel('Average Time (s)')
    plt.xticks(rotation=45)
    plt.legend()
    plt.tight_layout()
    plt.savefig(output_fig)  
    plt.show()

 

# Main function
def main():
    directory = "time_results"
    files = os.listdir(directory)
    real_means = []
    user_means = []
    sys_means = []

    output_fig = "time.png"

    output_file = "timeS.txt" 
    if os.path.exists(output_file):
        os.remove(output_file)

    for file in files:
        if file.endswith(".txt"):
            real_mean, user_mean, sys_mean = process_file(os.path.join(directory, file),  output_file )
            real_means.append(real_mean)
            user_means.append(user_mean)
            sys_means.append(sys_mean)

    plot_results(real_means, user_means, sys_means, files, output_fig)

if __name__ == "__main__":
    main()
