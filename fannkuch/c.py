import re
import pandas as pd
import matplotlib.pyplot as plt

# Créer une liste vide pour stocker les dictionnaires de données
data = []

# Ouvrir le fichier "timeS.txt" en mode lecture
with open('timeS.txt', 'r') as file:
    # Lire chaque ligne du fichier
    for line in file:
        # Utiliser une expression régulière pour extraire le nom du langage
        langage_match = re.match(r'time_results/time_results_(\w+).txt:', line)
        if langage_match:
            langage = langage_match.group(1).capitalize()
        else:
            continue

        # Lire la ligne suivante pour obtenir les temps de résulat
        results_line = next(file)
        real_value = float(re.search(r'real:\s*(\d+\.\d+)', results_line).group(1))
        user_value = float(re.search(r'user:\s*(\d+\.\d+)', results_line).group(1))
        sys_value = float(re.search(r'sys:\s*(\d+\.\d+)', results_line).group(1))

        # Créer un dictionnaire de données pour le langage actuel
        langage_data = {
            'langage': langage.strip(),
            'real': real_value,
            'user': user_value,
            'sys': sys_value
        }

        # Ajouter le dictionnaire de données à la liste
        data.append(langage_data)

# Créer un DataFrame Pandas à partir de la liste de dictionnaires de données
df = pd.DataFrame(data)

# Filtrer le DataFrame pour inclure uniquement les langages C++, C, Java et Ruby
df_filtered = df[df['langage'].isin(['Cpp', 'C', 'Java', 'Ruby'])]

# Afficher le DataFrame filtré
print(df_filtered)

# Tracer le graphique à barres
bar_width = 0.35
index = range(len(df_filtered['langage']))
plt.bar(index, df_filtered['real'], bar_width, label='Real')
plt.bar([i + bar_width for i in index], df_filtered['user'], bar_width, label='User')
plt.xlabel('Langage')
plt.ylabel('Temps d\'exécution (s)')
plt.title('Temps d\'exécution par langage')
plt.xticks([i + bar_width / 2 for i in index], df_filtered['langage'])
plt.legend()
plt.show()


# Filtrer le DataFrame pour inclure uniquement les langages C++, C++ o2, C++ o3
df_filtered1 = df[df['langage'].isin(['Cpp', 'Cppo2', 'Cppo3'])]

# Afficher le DataFrame filtré
print(df_filtered1)

# Tracer le graphique à barres
bar_width = 0.35
index = range(len(df_filtered1['langage']))
plt.bar(index, df_filtered1['real'], bar_width, label='Real')
plt.bar([i + bar_width for i in index], df_filtered1['user'], bar_width, label='User')
plt.xlabel('Langage')
plt.ylabel('Temps d\'exécution (s)')
plt.title('Temps d\'exécution par langage')
plt.xticks([i + bar_width / 2 for i in index], df_filtered1['langage'])
plt.legend()
plt.show()

# Filtrer le DataFrame pour inclure uniquement les langages C++, C++ o2, C++ o3
df_filtered2 = df[df['langage'].isin(['C', 'Co2', 'Co3'])]

# Afficher le DataFrame filtré
print(df_filtered2)

# Tracer le graphique à barres
bar_width = 0.35
index = range(len(df_filtered2['langage']))
plt.bar(index, df_filtered2['real'], bar_width, label='Real')
plt.bar([i + bar_width for i in index], df_filtered2['user'], bar_width, label='User')
plt.xlabel('Langage')
plt.ylabel('Temps d\'exécution (s)')
plt.title('Temps d\'exécution par langage')
plt.xticks([i + bar_width / 2 for i in index], df_filtered2['langage'])
plt.legend()
plt.show()