#!/bin/bash

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_python.txt"
#POWER_FILE="power_results/power_results_python.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# Python implementation
#for i in {1..12}
#do
    # Measure the time using built-in time and append to time_results.txt
 #   (time python3 f.py 12) 2>> $TIME_FILE

#done

#for i in {1..5}
#do
#     python3 f.py 12 &
#     PYTHON_PID=$!
#     powerjoular -p $PYTHON_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 600
#     kill -INT $POWERJOULAR_PID
#     kill $PYTHON_PID
#done


############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_c.txt"
#POWER_FILE="power_results/power_results_c.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# C implementation
#gcc f.c -o fC
#for i in {1..12}
#do
 #   (time ./fC 12) 2>> $TIME_FILE
#done

#for i in {1..5}
#do
#    ./fC 12 &
#    PROGRAM_PID=$!
#    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#    POWERJOULAR_PID=$!
#    sleep 60
#    kill -INT $POWERJOULAR_PID
#    kill $PROGRAM_PID
#done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_cO2.txt"
POWER_FILE="power_results/power_results_cO2.txt"

# Clean any existing result files
#> $TIME_FILE
> $POWER_FILE

# C implementation
gcc -o2 f.c -o fC
#for i in {1..12}
#do
#   (time ./fC 12) 2>> $TIME_FILE
#done

for i in {1..8}
do
    ./fC 12  &
    PROGRAM_PID=$!
    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
    POWERJOULAR_PID=$!
    sleep 65
    kill -INT $POWERJOULAR_PID
    kill $PROGRAM_PID
done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_cO3.txt"
POWER_FILE="power_results/power_results_cO3.txt"

# Clean any existing result files
#> $TIME_FILE
> $POWER_FILE

# C implementation
gcc -o3 f.c -o fC
#for i in {1..5}
#do
#   (time ./fC 12) 2>> $TIME_FILE
#done

for i in {1..8}
do
    ./fC 12 &
    PROGRAM_PID=$!
    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
    POWERJOULAR_PID=$!
    sleep 65
    kill -INT $POWERJOULAR_PID
    kill $PROGRAM_PID
done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_cpp.txt"
#POWER_FILE="power_results/power_results_cpp.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# C++ implementation
#g++ f.cpp -o fCpp

#for i in {1..12}
#do
    #(time ./fCpp 12) 2>> $TIME_FILE
#done

#for i in {1..5}
#do
#    ./fCpp 12 &
#    PROGRAM_PID=$!
#    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#    POWERJOULAR_PID=$!
#    sleep 175
#    kill -INT $POWERJOULAR_PID
#    kill $PROGRAM_PID
#done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_cppO2.txt"
POWER_FILE="power_results/power_results_cppO2.txt"

# Clean any existing result files
#> $TIME_FILE
> $POWER_FILE

# C++ implementation
g++ -o2 f.cpp -o fCppO2

#for i in {1..5}
#do
#    (time ./fCppO2 12) 2>> $TIME_FILE
#done

for i in {1..5}
do
    ./fCppO2 12 &
    PROGRAM_PID=$!
    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
    POWERJOULAR_PID=$!
    sleep 180
    kill -INT $POWERJOULAR_PID
    kill $PROGRAM_PID
done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_cppO3.txt"
POWER_FILE="power_results/power_results_cppO3.txt"

# Clean any existing result files
#> $TIME_FILE
> $POWER_FILE

# C++ implementation
g++ -o3 f.cpp -o fCppO3

#for i in {1..5}
#do
#    (time ./fCppO3 12) 2>> $TIME_FILE
#done

for i in {1..30}
do
    ./fCppO3 12 &
    PROGRAM_PID=$!
    powerjoular -p $PROGRAM_PID >> $POWER_FILE &
    POWERJOULAR_PID=$!
    sleep 180
    kill -INT $POWERJOULAR_PID
    kill $PROGRAM_PID
done

############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_java.txt"
#POWER_FILE="power_results/power_results_java.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# Java implementation
#javac Fannkuch.java

# for i in {1..5}
# do

#     (time java Fannkuch 12) 2>> $TIME_FILE
# done

# for i in {1..5}
# do
#     java Fannkuch 12 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 28
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done


############################################
############################################
############################################

# Name of the files to store results
#TIME_FILE="time_results/time_results_ruby.txt"
#POWER_FILE="power_results/power_results_ruby.txt"

# Clean any existing result files
#> $TIME_FILE
#> $POWER_FILE

# Ruby implementation
# for i in {1..12}
# do

#     (time ruby f.rb 12) 2>> $TIME_FILE
# done

# for i in {1..2}
# do
#     ruby f.rb 12 &
#     PROGRAM_PID=$!
#     powerjoular -p $PROGRAM_PID >> $POWER_FILE &
#     POWERJOULAR_PID=$!
#     sleep 1200
#     kill -INT $POWERJOULAR_PID
#     kill $PROGRAM_PID
# done
